FOR1  ?�BEAMExDc  ~�hd elixir_docs_v1l   hd docsl   hhd __behaviour__aad defl   hd atom1jd Elixirjd falsehhd breakab  cd defl   hd refjd nilhd timeoutjd niljm  �Breaks the active connection.

Any attempt to use it inside the same transaction
Calling `run/1` inside the same transaction or run (at any depth) will
return `{:error, :noconnect}`.

## Examples

    Pool.transaction(mod, pool, timout,
      fn(:opened, ref, conn, _queue_time) ->
        :ok = Pool.break(ref, timeout)
        {:error, :noconnect} = Pool.run(mod, pool, timeout, fn _, _ -> end)
      end)

hhd rollbackab  Cd defl   hd pool_modjd nilhd pooljd nilhd valuejd niljm   \Triggers a rollback that is handled by `with_rollback/2`.

Raises if outside a transaction.
hhd runaa�d defl   hd pool_modjd nilhd pooljd nilhd timeoutjd nilhd funjd niljm  �Runs a fun using a connection from a pool.

The connection will be taken from the pool unless we are inside
a `transaction/4` which, in this case, would already have a conn
attached to it.

Returns the value returned by the function wrapped in a tuple
as `{:ok, value}`.

Returns `{:error, :noproc}` if the pool is not alive or
`{:error, :noconnect}` if no connection is available.

## Examples

    Pool.run(mod, pool, timeout,
      fn(_conn, queue_time) -> queue_time end)

    Pool.transaction(mod, pool, timeout,
      fn(:opened, _ref, _conn, _queue_time) ->
        {:ok, :nested} =
          Pool.run(mod, pool, timeout, fn(_conn, nil) ->
            :nested
          end)
      end)

hhd transactionaa�d defl   hd pool_modjd nilhd pooljd nilhd timeoutjd nilhd funjd niljm  {Carries out a transaction using a connection from a pool.

Once a transaction is opened, all following calls to `run/4` or
`transaction/4` will use the same connection/worker. If `break/2` is invoked,
all operations will return `{:error, :noconnect}` until the end of the
top level transaction.

Nested calls to pool transaction will "flatten out" transactions. This means
nested calls are mostly no-op and just execute the given function passing
`:already_opened` as first argument. If there is any failure in a nested
transaction, the whole transaction is marked as tainted, ensuring the outer
most call fails.

Returns `{:error, :noproc}` if the pool is not alive, `{:error, :noconnect}`
if no connection is available. Otherwise just returns the given function value
without wrapping.

## Examples

    Pool.transaction(mod, pool, timeout,
      fn(:opened, _ref, _conn, queue_time) -> queue_time end)

    Pool.transaction(mod, pool, timeout,
      fn(:opened, ref, _conn, _queue_time) ->
        :nested =
          Pool.transaction(mod, pool, timeout, fn(:already_opened, _ref, _conn, nil) ->
            :nested
          end)
      end)

    Pool.transaction(mod, :pool1, timeout,
      fn(:opened, _ref1, _conn1, _queue_time1) ->
        :different_pool =
          Pool.transaction(mod, :pool2, timeout,
            fn(:opened, _ref2, _conn2, _queue_time2) -> :different_pool end)
      end)

hhd with_rollbackab  "d defl   hd atom1jd Elixirhd refjd nilhd funjd niljm   �Executes the given function giving it the ability to rollback.

Returns `{:ok, value}` if no transaction ocurred,
`{:error, value}` if the user rolled back or
`{:raise, kind, error, stack}` in case there was a failure.
jhd 	moduledocham   +Behaviour for using a pool of connections.
j  Atom  `   7Elixir.Ecto.Pool__info__	functionsmodulemacrosclose_transactionconnworkerokwith_rollbackopenedalready_openElixir.Processgettaintedfalsetruethrowecto_rollbackerrorerlangget_stacktraceraiserollbacknilElixir.RuntimeError	exceptioncheckout	noconnectnoproc__exception__
__struct____behaviour__docs	callbacksbehaviour_infoputbadmapbreakmapsremovetransactionopen_transactionouter_transactiondo_runinner_transactionrunoptional_callbacksmodule_infoget_module_info-do_run/4-after$^0/0-0-checkin"-outer_transaction/6-after$^0/0-0-delete-with_rollback/3-after$^0/0-0-Code  	          �   ^   � " 0e;e`25BERU0@G @@P@`Wp�b0�9�:�0B 3BCB S+�3�u��r�� �c p@C3@c@bC@S� p0@� ��u@���0�0�0�;�@����@0h4h$@#@�@K @@�P �� �0;@
�
��0 F G�G=��@G�i$=j$+
9:0B 3BCB S+3
+C0`F G
GS=@@$�` PF@G
G$GG@i4@4@�0�p]@j4@@#$@4�  �p]� l  0h@#@��K 0F G�G@i j+
9:0B 3BCB S+3
+C@
@@S�� +0 F G
G � l# �PJ��
0 @0@#F03GGG@3@3�� ,
+
@G �� ��0@ F0G
GG��@��
0 0@
3@C@#@#@C��p 9%<%@@ B +%�= B B#+%
, #
+!#
  !5"#@#� �0"0##@#@
#@�p�0#�$#�$#@

 +$
0$@#�0$�J#%��J&�
!'0&;&@
"(
#)(@G0)�NP*�� +  @@� �,�� �@�  � `@� ,0F G
&G@�0-�
' .9-:-0B #B3B C+-#PP@@$@C4@3D� �0�/r�/ �@@r� p@@$$� `@#@D3@@
'C@4�q0P/@�P0�J1�
*@2P@@@3D@#4@$@F0GGG@� �3�< r@D#@0IP3+<
@#@4@
+3@$$�p 9;<;@ 4@94B B#+;
,5#
+6#
5P657#4D@#� �0708#@#@
#4D@�p�08�:#�:#@

 +:
0:@#�09B B#B 3B0C+;�@#@3#@C3@4C@DS@`>P:�J#;�J<�J=�
,`>p`��G@``r#�
�@S$@C4@3D@#T@d� `hAh?@T#@D3@d@$C$DT@��K@i@T@d�  @@4#@d� 0�@T=@?j@D@T�! @$@4@d�" .@D@$#@T$DT�#0�@i@T�P @�$[@ Aj@d@#D@T�@0�$[� l0B�%
-@C`@@3$@#4@D@T�&09G<G@ D@EDB +G
`EB B#B 3B0C+G�hF@#S@C@$#@3@S$�'K 0F G�G@i@@T#@D@$3@4�(@Y@`Fj@@#@T#@D@$3@4�(@Y� l`G�&JH�)
.0I@0h4J@
3@#C@#@@�@$�*K@i4@Jj4@@4�+ @@
@$�, +@@#@4�-0�@K�.
/@L@@@@34@#$@@F0GGG�/ �N�M@r#�+M
@
@#@4#�@ �0K 0F G�G@ M@GP@N+O
@$#@@43@@C@O�/JP� 
$Q0P;P@
#R
0SR@G`S@T� 
1 U@� N�V� 
1W@@� N �X�(
3@Y @@
4C@S@3@#3@#@S�1q0 Z�$
5[�2N�\�p
7]@
 +StrT    ImpT   �                                                       $         %      (   )                  2         2         6   ExpT   |   
   1      W   1       U   $      Q   /      L   *      2   '      .   !      '            
               LitT  �  	�x��V͎#5�����Jp�kݖ�lF3��n�			�;ݕio���3��s�x ��@��Nwf����C�v�����rwUUO�����ܞ5ճ�z���ծ��z�{�o<��:���w�lT�S3~��ު�x�4ze���ve�ٝ��z�Mg||����O����~O�_��Z�V��`�Ǡ���)Uu�[��!*�F[ª.	\�[Jz��{6���9�Ijg-�a!�ܻ�7(A�-N��-s�ku�Bh]o���"�
�U��e���>,V�H�*`�a�6 ]��t�)��a�y�L���y�a��B�u�����pw��@�^
�9ȥU{���ɋ׉�����r�ϣ_�ߠ@D;����0�����������	DO"����#���9�x���GPt&Wǭ�`΃���2�HA�7Ke��i]�2 �W�2Ka�+O�*@���h�p��R9�[�w$+ʫ�P��*۔�����������^k�4��T�Jf10O�w5� �#����a�'�ǔ���Ϡ��g?���:�=�v�5gg�"�Lf�A�"?)u�$�Iئ��E�;����t~��<�9�(A�/�~${H(��r�T��x˅�4�
�db/���h!����j��1[dCp�`e�m�sP��5m�w��Q���$!h�fꛃ�_Wy��hB&"XQg�\b<�]�0$�>L��-R[�LW���2�:%��4����w��Gp���ܣ�{�qz)���9�[�(m����dn�S2���e��@[*ޝ���Cס]O�g�'�o�L�N��=��?�����_L��9e�>�b��(�?Vl��3��Uf�@nY�W�����]�)p�>�?�l���y�Xf��tL�E�n���j��u��>V����w���ԍ[G_,���p|�������b�  LocT   p   	   7      ]   5      [   3      Y   .      I   -      C   ,      >         +                  Attr  �l   hd vsnl   n �O3DiF߲*`����s�jhd typedocl   hhd refa m   �Opaque connection reference.

Use inside `run/4` and `transaction/4` to retrieve the connection module and
pid or break the transaction.
jhd typedocl   hhd ta m   A pool process
jhd typedocl   hhd deptha m   "The depth of nested transactions.
jhd typedocl   hhd 
queue_timea m   GThe time in microseconds spent waiting for a connection from the pool.
jhd callbackl   hhd 
start_linkal   hd typek 1d bounded_funl   hd typek 1d funl   hd typek 1d productl   hd typek 1d modulejhd vark 1d optsjhd typek 2d unionl   hd typek  d tuplel   hd atomk  d okhd typek 2d pidjjhd typek  d tuplel   hd atomk  d errorhd typek 2d anyjjjjl   hd typek 1d 
constraintl   hd atomk 1d 
is_subtypel   hd vark 1d optshd remote_typek 2l   hd atomk  d Elixir.Keywordhd atomk  d tjjjjjjjjhd callbackl   hhd open_transactional   hd typek dd bounded_funl   hd typek dd funl   hd typek dd productl   hd 	user_typek dd tjhd typek dd timeoutjjhd typek ed unionl   hd typek ed tuplel   hd atomk  d okhd vark ed workerhd vark ed connhd 	user_typek ed 
queue_timejjhd typek  d tuplel   hd atomk  d errorhd typek fd unionl   hd atomk  d noprochd atomk  d 	noconnectjjjjl   hd typek dd 
constraintl   hd atomk dd 
is_subtypel   hd vark dd workerhd typek fd anyjjjhd typek dd 
constraintl   hd atomk dd 
is_subtypel   hd vark dd connhd typek  d tuplel   hd typek fd modulejhd typek fd pidjjjjjjjjhd callbackl   hhd checkoutal   hd typek Bd bounded_funl   hd typek Bd funl   hd typek Bd productl   hd 	user_typek Bd tjhd typek Bd timeoutjjhd typek Cd unionl   hd typek Cd tuplel   hd atomk  d okhd vark Cd workerhd vark Cd connhd 	user_typek Cd 
queue_timejjhd typek  d tuplel   hd atomk  d errorhd typek Dd unionl   hd atomk  d noprochd atomk  d 	noconnectjjjjl   hd typek Bd 
constraintl   hd atomk Bd 
is_subtypel   hd vark Bd workerhd typek Dd anyjjjhd typek Bd 
constraintl   hd atomk Bd 
is_subtypel   hd vark Bd connhd typek  d tuplel   hd typek Dd modulejhd typek Dd pidjjjjjjjjhd callbackl   hhd breakal   hd typek Td bounded_funl   hd typek Td funl   hd typek Td productl   hd 	user_typek Td tjhd vark Td workerhd typek Td timeoutjjhd atomk  d okjl   hd typek Td 
constraintl   hd atomk Td 
is_subtypel   hd vark Td workerhd typek Td anyjjjjjjjhd callbackl   hhd checkinal   hd typek Ld bounded_funl   hd typek Ld funl   hd typek Ld productl   hd 	user_typek Ld tjhd vark Ld workerhd typek Ld timeoutjjhd atomk  d okjl   hd typek Ld 
constraintl   hd atomk Ld 
is_subtypel   hd vark Ld workerhd typek Ld anyjjjjjjjhd callbackl   hhd close_transactional   hd typek od bounded_funl   hd typek od funl   hd typek od productl   hd 	user_typek od tjhd vark od workerhd typek od timeoutjjhd atomk  d okjl   hd typek od 
constraintl   hd atomk od 
is_subtypel   hd vark od workerhd typek od anyjjjjjjjj CInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek :/Users/davidwalker/erlang/rumbl/deps/ecto/lib/ecto/pool.exj  Abst  �P  ]�x��\ݏ�q߽����E"�A/��%:���iC"`��FA��a_�g�fzogoof=3�~�[ ;$C�y �PLE1���4J��� !a��pw�tOT�Βg?y�������uuu���g������v��(���+�F��~+l,�<O��q�Q#l���(��l��h�'~�7J�\#m��V���gvG���q���Q���y�4��{I8�|�l��Q��A,���h�L��/�p���>�|6�=��#����G��c�I���$�HNq��N�2�Eq������ɮ����%�b��-O�(2>����p+d��:�(����N1�ed��&�.s3/$>�=����I��L��>n.ģ�_�}q���~{Iϋq��P�"3�xAg�!�c?��p��o)O��(�ۋ�4�0Y�y�ǹwEy�x=�t$k��&{L�����f��<��O��jV������t��-�\Q:�zE� "~;a���BS�e@����h�X���ʓ=�W@$�Q%����N}���@'l$���W�H��8&���(�L���4KPk�~^�]�D��I��xǏ������4^�c��L~N�	�ĞǨUXG�!$���8q�����I�,�:O6W8r��Rz,&�93�ʃ.(k���a�9�R�>�_R�t��*
�f�h8Ȝ�)���| ��02^g�3ɮ`���i��s8M��:�Lڎ��8)�	����{��ң��GcH;�����|?�t��@���
�Xn���in+x]������N��qtۊ��V���p{[�-�I9�D��T-AH�Ϣ�Oc��S�ǂ	EX��z��%TT�١r��R-�;�Y�X�r�����8T��
I�^�i�n�;�!S��;.�q���í<F�5w�o��[׎[	�4n�`�_���|ŻhF�t+�M�ZwEw�r��������Q�I�x2�+�/��D>��F�������'_#mXL�^��b'�p��2��{|�\��pg!`�а��!|ٝ���Aˆ�e�j�3�<�Ԍ-�Ų�Z�3����fvq*7��"Ƀ��dp�=��������Z�,X�Ӄ�����A���8�:}�X�\u�rD���*WaT�YP�V�k&*�`T�*���.Ь:@�
�r@��LP��*�]��2P9ǲhV���[ $�TH�j�* ɗ��qKA��:�`�r)xˡ`����3��i(m�m�(6Զ�T�&��M��6U�m�oN��M��6j��lU[b��K�0ɰr@�
L�
L@&n&�`�4
L\
L
��lS�6�u[�)�{U;>J:S1e���CمAfW �WA�9�}?M
��F.�a��S�<���Xz����6�S*<Y�j!5�(%��I̒E+�� �借#�ΰ�D�*���u:r��<��_��`��0�Nh��ȾR��f�����l��S�G,{������|˰�
�U����@ ���.ͩ��&谚*�U�
2�~H{G̞)����6�R)�$Ev_�-�0jc{���V����
�2mF�b(��9H�r��/��G3B���!";5�I��P(C��R��,�̈́*F��LE�/Ө����|
hj�g�p�X��gA��6�U��m�����̵�y'ZYq[��,7��JX�]F���XO$�)Pi������@E7u����ӌ$#��P���0I��C�q>��+ZO��|w�8��HL���a���"<}|���z���ۍʳ���]7�ч�V.SձP1�G� ��������U��F1T��(�LcKJMi�BZ�]��4?g��ӡd��F7�"����"��P{ф���_�1�륍dFn�f�M�����ka��=�}��Uy�W��`�g�S6�NT�P�2���xI��K`Z�E�]�Kd��+[�l/V;�4�rE_��e�- ����3��+����gǑcVJ�)2�1v���Sԣ�E��aVSV->Z�|P�sT�_�؊6�sy?M�M${�~v�L\�W @�"<1���Λ�<|��%��}���.~a��ٍb͋/Q�c�%��.��6�1�������<�m�c�o���;�6"��%�ȗ�uI���$�2��e�*�+��iM���H��
��}�쫐`�\��Id�U�|��I���V�=%/��2�oW�9{U�W��Zo{d���*��W�������{M��k+�Z�����S��2��5�&
)z���k�xl�2�d)�ͼ.��˯l�>e̸n�q]��xC�׆�;6j���x�yo@��~I���IFeg;I1��j��wN}`�޳�Ed�Q܍��uMU�Ot��s�N��˥�8�ix��%��3:�J���_��hN7�}<�{8΋�P;��(�A�����I�Q�<�3<��-���a"�u�{h<̫���m[���Ok�?=Q�������i�i�ѝv:�+��hx�+�uPj`O�f�i�&����ݬn�7%}�|�M��Dۅ�j:��)�ME�b)���R��+y�w�8A.U�KX��ŋ����:��Q)���6���J3��?x�,�{���Ms�����e ��3:�A�^�;���(\�٫2��C�gZ�R�SU�˯���ѯ�#P#�n	�[��@�-U�[����%����	P>Q�S�^��0�@S򇼵���O3�'�
<Qq'Bq�Dq�ox�����9�Z����~�-~�_��.0����ɭ'y�E�d:Yc�3�/d- ����n�<P���Cga�,���(�W�3C�,��\p�K�x��BI삓N��&��*�g�ݑJ8�v�Ơ6h�*	���fD'�P�"��l��l���9�e�%�\?
�^�O���#D1{A��2}/D�������F���8�cî�R�Ey�}���W%G���$#.N�p��,�h�����s��JI}���W�����y����ť%����?Y���n���SM��.�#�JF0���Uy3���S��7�u=�fD*O�>F�ef�+g2"��"s����z|p���]A�-�t;�"�OO�,DIV@����`6,�+g���+G�8��:p���@��$�>-����S����q�;�Ζ��N��Y} ��P�V,�jy�<�(U+��S����+��.
��͒W}PW�eP�t3a��5�u��GK�l�P&�y'�1���#��8�i�ז�����#QLW�����2���Ew���+]ӧ��R5�*���8ʀ�k1Fyc�ǝE�0�⃇)�&L嬎�ͧEi�׊�@��"At�����0j���!��P�b$�6��������J��O+�Bac�)��\�8>x偵pCu[O��]o.�q��g�<�̔.��9���!�������a�ɖ��1�$�;8x}_�O��S����>��@R�';7\o=->���������iV�'�������9�'�̸Y��!߰���U@T��FR�S�!�q�A�8 ��^����z����K�Xy���v���rO	Gm��tq5*�U-�L��O7��&���_oy�2Y��ȴgX�H��ђ�g��r�g,9�3"�L�6��'z��i�˲��H�����S�%;��$r�\�#Ԭ�7���ªm��I�`��O`����BU��������b�P�hXۢ�:��\���k�IA`�
+��@a�'P�X�(l��>��K�'���B�=MŽ�*�Y6MO�45�i���Y�hC�=Iʬ;ڬ;���_;|U���c��kGŻ����M`�~KO�݅_�Ki���DfY��p��Y��+K-��&H0C����:3z
WC@R����UR�9��A-� A-��i�⵴V��W�uO래䞥�v��Ղ�=�Z�����̇�"ڇ �ic[+�Ѧ��f�X�W��HF_d�kjN������2�7e�/d�� �ޯ�+�恞�0�
���E��"����@��=�_ī=�R��K�w��~g/���}ko_l�Q�ۗ�Ծ�L��o/��)�N��F9x��x��"^%�D��E�)ׁ�!��s:�d+���[|���E\�S凊x��"�:�~o�Q�;@�E��0�.�@�T}�D9�'����Q��{ zW�>&�MD|�q�T��|l7�
��PR��BGj��C��{h��:����C�{s��P�#�����G�� �6J��}$��AQ�%�6��o�˟<\�ãJM��:?T�آ�k��DL�	׫:io!�j�OL�~"�Nl���?0�`�/*A���Z��h�2�@��@�4��I{����FY��z�񩺂O��i���7m�K�*�������tK����?�h�>��>���&�� �窺�ǲP��B=�_ͫj{l��1�����ohw�F-�0J2.�4ij�c9���JG%����$�\U�W�[�U�tt�W���E�z��ŵj�״/+m_�%�fJr͸�)����n�]����n�۸��a��� �-��-{��Y��my~�(�!���t�ra�Q�dU3�0�}R�5A�.���;S݈�anH�)�Z�5���. �ۂ��YA�HX��<l˰'�==�3B[۶���/mH��"��i߬�`+�=2�`�
>:�!�kA�[�<�ZМ��u���G
��(��S��멋�rQrq����S� �]��wQ9k�1T�ë�'/�~�"�m/���"��/�s���% �yi���%X�KPN�r�/˾�r���2�]Vr�J��_��4�,W*�+�\Vmq^��W@[\�sto���7��7ׅ7m�h聼k�{K�|H���x�.~���O�"X��i��� ��� Line   �           Z   2   )))")$)%)-)#)6)9)C)E)H)F	�	�	�)L)M)N)c)d)f)g	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�)))	)
)	�	�	�	�)  lib/ecto/pool.ex  