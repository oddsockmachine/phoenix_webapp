FOR1  1�BEAMExDc   R�hd elixir_docs_v1l   hd docsl   hhd create_hashaa�d defl   hd passwordjd nilhd \\jl   hd optsjd niljjjm  A function that provides options to check the strength of a password
before hashing it. The password is then hashed only if the password is
considered strong enough. For more details about password strength,
read the documentation for the Comeonin.Password module.

The default hashing algorithm is bcrypt, but this can be changed to
pbkdf2_sha512 by setting the value of `crypto_mod` to `:pbkdf2`
in the config file.

## Options

There are three options:

  * min_length -- minimum allowable length of the password
  * extra_chars -- check for punctuation characters and digits
  * common -- check to see if the password is too common (easy to guess)

The default value for `min_length` is 8 characters if `extra_chars` is true,
but 12 characters is `extra_chars` is false. `extra_chars` and `common` are
true by default.

## Examples

The following examples will produce password hashes:

    Comeonin.create_hash("longpassword", [extra_chars: false])

    Comeonin.create_hash("passwordwithjustletters", [min_length: 16, extra_chars: false])

This example will raise an error because the password is not long enough for a password
with no punctuation characters or digits:

    iex> Comeonin.create_hash("password", [extra_chars: false])
    {:error, "The password should be at least 12 characters long."}

This last example will raise an error because there are no punctuation characters or
digits in it:

    iex> Comeonin.create_hash("password")
    {:error, "The password should contain at least one number and one punctuation character."}

This example will raise an error because the password is too similar to the common
password `password`:

    iex> Comeonin.create_hash("p4$5w0rd")
    {:error, "The password you have chosen is weak because it is easy to guess. Please choose another one."}

hhd create_useraa�d defl   hd user_paramsjd nilhd \\jl   hd optsjd niljjjm  �This function takes a map with a password in it, removes the password
and adds an entry for the password hash. This can be used after collecting
user data and before adding it to the database.

This uses the `create_hash` function, which can be used to check password
strength before hashing it.

When looking for the password, this function looks for a key which is either
named "password" (a string) or :password (an atom). If it does not find
either key, it will raise an error.

As with the `create_hash` function, you can decide not to check password
strength by setting the second argument to false.

## Examples

All of the following will work ok:

    %{"name" => "fred", "password" => "&m@ng0es"} |> Comeonin.create_user

    %{name: "fred", password: "&m@ng0es"} |> Comeonin.create_user

    %{name: "fred", password: "longpassword"} |> Comeonin.create_user([extra_chars: false])

The next example will raise an error because the key "password" or :password
could not be found:

    iex> %{["name"] => "fred", ["password", "password_admin"] => "&m@ng0es"} |> Comeonin.create_user
    {:error, ~s(We could not find the password. The password key should be either :password or "password".)}

This example will raise an error because the password is not long enough:

    iex> %{name: "fred", password: "123456"} |> Comeonin.create_user
    {:error, "The password should be at least 8 characters long."}

hhd time_bcryptaaad defl   hd \\jl   hd 
log_roundsjd nilajjm  A function to help the developer decide how many log_rounds to use
when using bcrypt.

The number of log_rounds can be increased to make the bcrypt hashing
function more complex, and slower. The minimum number is 4 and the maximum is 31.
The default is 12, but this is not necessarily the recommended number.
The ideal number of log_rounds will depend on the nature of your application
and the hardware being used.

The `bcrypt_log_rounds` value can be set in the config file. See the
documentation for `Comeonin.Config` for more details.
hhd time_pbkdf2aasd defl   hd \\jl   hd roundsjd nilb  �`jjm  �A function to help the developer decide how many rounds to use
when using pbkdf2.

The number of rounds can be increased to make the pbkdf2 hashing function slower.
The maximum number of rounds is 4294967295. The default is 60_000, but this
is not necessarily the recommended number. The ideal number of log_rounds
will depend on the nature of your application and the hardware being used.

The `pbkdf2_rounds` value can be set in the config file. See the
documentation for `Comeonin.Config` for more details.
hhd time_strongaa�d defl   hd \\jl   hd passwordjd nilm   passwordjjm   �This is a development tool to see how quickly, or slowly, the common password
function is running.

Unless you are contributing to Comeonin development, you will not need to use
this function.
jhd 	moduledocham  Comeonin is a password hashing library that aims to make the
secure validation of passwords as straightforward as possible.

It also provides extensive documentation to help
developers keep their apps secure.

Comeonin supports bcrypt and pbkdf2_sha512.

## Use

This module offers the following convenience functions:

  * create_hash -- check password strength before hashing it
  * create_user -- update a map with the password hash

See the documentation for each function for more details.

If you do not need this extra functionality, you can hash a password
by using the `hashpwsalt` function -- using either Comeonin.Bcrypt or
Comeonin.Pbkdf2.

See each module's documentation for more information about
all the available options.

If you want more control over the generation of the salt, and, in
the case of pbkdf2, the length of salt, you can use the `gen_salt`
function and then pass the output to the `hashpass` function.

To check a password against the stored hash, use the `checkpw`
function. This takes two arguments: the plaintext password and
the stored hash.

There is also a `dummy_checkpw` function, which takes no arguments
and is to be used when the username cannot be found. It performs a hash,
but then returns false. This can be used to make user enumeration more
difficult. If an attacker already knows, or can guess, the username,
this function will not be of any use, and so if you are going to use
this function, it should be used with a policy of creating usernames
that are not made public and are difficult to guess.

## Choosing an algorithm

Bcrypt and pbkdf2_sha512 are both highly secure key derivation functions.
They have no known vulnerabilities and their algorithms have been used
and widely reviewed for at least 10 years. They are also designed
to be `future-adaptable` (see the section below about speed / complexity
for more details), and so we do not recommend one over the other.

However, if your application needs to use a hashing function that has been
recommended by a recognized standards body, then you will need to
use pbkdf2_sha512, which has been recommended by NIST.

## Adjusting the speed / complexity of bcrypt and pbkdf2

Both bcrypt and pbkdf2 are designed to be computationally intensive and
slow. This limits the number of attempts an attacker can make within a
certain time frame. In addition, they can be configured to run slower,
which can help offset some of the hardware improvements made over time.

It is recommended to make the key derivation function as slow as the
user can tolerate. The actual recommended time for the function will vary
depending on the nature of the application. According to the following [NIST
recommendations](http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf),
having the function take several seconds might be acceptable if the user
only has to login once every session. However, if an application requires
the user to login several times an hour, it would probably be better to
limit the hashing function to about 250 milliseconds.

To help you decide how slow to make the function, this module provides
convenience timing functions for bcrypt and pbkdf2.

## Further information

Visit our [wiki](https://github.com/elixircnx/comeonin/wiki)
for links to further information about these and related issues.

j  Atom  �   *Elixir.Comeonin__info__	functionsmodulemacroscreate_user
create_maperrorok
Elixir.Mapput_newtime_strongcreate_hashtime_pbkdf2Elixir.Comeonin.Pbkdf2gen_salthashpasstimertc
Elixir.MixshellElixir.String.Chars	to_stringerlangdiv	byte_sizeallinfopasswordmapsremovepassword_hashElixir.Comeonin.Configget_crypto_modElixir.Comeonin.Passwordstrong_password?true
hashpwsalttime_bcryptElixir.Comeonin.Bcryptmodule_infoget_module_info   Code  �          �   4   � " 0e;e`25BERU0@G @@P@`3p�b�@ �� r@� @@@@3@#�0  9�:� B B#0�;�@�����00F G�G# �@@�  �@0 0F G�G@ Й0J��P� �@G#�`�@  �p�0@$�� `E$3E3EG#@
@���0 9: B �� 0@5$@$=@$$��@��}Pi�@$5@=@��@��@| `#o#| `$#o#o	m  \� Z
� $\��Z
� \0@
#@@��q0��H��b �� 
# 0@@@
@#�� p@
 #@3@@� � G# 0@@@G@#�� p@G #@3@@� @G0�p� @9 �`�`�    @@�� �@#@@@#�� �+!
%@@
&#@�  �p0F G�G@ !0F G�G@ "�P�# EG@#@
$@
#�0 9&:& B � 0�}Pi�@5$@=%$@�@%�@| `#o#o�m  \`�Z
� \0@
#@@�q&�H'�
' (@�*)�
'*0@$��@EEG#@
@
(�0 9/:/ B � 0@5+$@$=,+@$$�@,�}Pi�@$5-@=.-@�@.�@| `#o#| `$#o#o	m  \�Z
� $\��Z
� \0@
#@@�q0/�H0� 
) 1@� N�2� 
)3@@� N � StrT   Rounds: , Time:  msLog rounds:  ImpT   �      
                                                                          !   "       #   $      (            *         *   ExpT   �      )      3   )       1   '      *   '       (         #                                                                           LitT   �  �x�e�A
�0ESAE7^`� .\����L��im�2����˙F�7C��e�!�X��j_�j$�,gBG�B�0�#��\,���qeCi��u(�H�%mv��&v�$r�c�O��&I�/���I��^K^2H��A����l����\j	�8(J-�)����Qg��UA���*��smVC�Z��۰դ/a%Ӧ�iM��N�O���\![���RU=�W{�LocT               
Attr   (�l   hd vsnl   n Tk��	gSs�&Z
��jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek =/Users/davidwalker/erlang/rumbl/deps/comeonin/lib/comeonin.exj   Abst  �P  &�x��YIo�F��K��E��G"��m�S.-��E�@/�������ͮ~����ؐ� ���.��(��� Er
�޼��������%�� f�`!c���4�sghk� ��53�fk�VĮm�A C��q� ��gk�1�s�ڞ���s0
�p�@Of���{?���M����
V8#
���l��a��82,޳4�:
J��d)Lx��R� 7��i%G1�D�Ք�<�ǏY�����F��))=O�4����#���8����P�u3Np,-$�od��'6����C���J6O,��sncK,��
l��Q �$iy�=��^����:��T�1�b��0 ᑗ�s6p�+��$	����
������&Y��PU��s'Y��6��	F�V��R�ݥ�q{[R����<A�v'R�x����ڢ��ٖ�����l.B�����հ/�u���ʻ/�$6�Q�)H{q��;|��v�Ж_�&Bx%6��c�+Y����I���M�Ҭp� �F��e�<�%�������EF�?��f~ىA�F�-�I1�OE1JE��������k� �^�u_�J
~Ư�fD��*�èR�[@,�Z
�ݐ��D�E�p|;%HqM�!}8CF��kNCn�W��F�]g6`�D	�w� �)����X#A��8�H =���q�P8^�/�Y��}�}��D߁�U��c�����l�8;J���槵����⚧�H)��F�[I��6��b�F �� D�	��N�F,���/�J��m]��5��Adz��L��܄+#�1`Ǥ���e�E�Gr4�I�hY�����x$7-Bϼ;���+��H��D�͔�f��f�HW�*wVӧ8��}��hC�C�HL�����_s���H(NW�����O��&�sr��)�A#����`�9z��,4���a�bP�A9������@r�N+SK8���X�.+��E����\�?w@�rI���z���Z�:g9/�YD�[����r�' 6H덉�۝�x`�E=����/� O>
@��ȓ� y�Q ��V �Aڮ)��w�(�L����}�q.s��P|I��?��}ۀ3`�����U��h��S��7�o��1N�U��ۋ��cw�Y8��)'&�ByP�����؟��ʹ�M��	�'������â9�;n������-�h�߲��rU��l��������l��R^�:�硱�%^^���k2��_]����o���V���FH���#�[���K���:9�	u�b�DF��a��p�]]�zy=J%2���8qm��?��8}��S��T���
����g����v�׶�"��4�؛r�z^�c�n�D�	DF-٣8�u��:�9_I;S2�s[���=�����М�Cs����B~��$���#.X�.��1�T�E������/�b��ӗ<�K~���p��Á���֋x<i�@O��D�@O�@���@O6�h�hS-��c8�>���,�0� �o[^��%����d�2����u��t�H_�����\�����ȵ���F֖9-:��2֖��-�߽vE�W�-��׊�h�� ���R����H8�h�`w�N�p�p��~41��M�k*�M6a��&���[�c2υ�U�hc��\�-��[B)�X{����j��(����:A��k��ۍ��%�V�\[Yrm�c�-x��S4ѓ.��Px��p>�^�<�.���*�"�=� (��-t_�ӣa�G�� �E���޿��	%   Line   R           1      	�	�	�	�	�	�	s	t	u	v	�	�	�	�	�	�	�	�	a	b	c	d lib/comeonin.ex  