FOR1  ?<BEAMExDc  �hd elixir_docs_v1l   hd docsl   hhd __before_compile__aa�d defmacrol   hd envjd Elixirjd falsehhd 	__route__ab  Gd defl   hd methodjd nilhd pathjd nilhd guardsjd nilhd optionsjd niljd falsehhd 	__using__aa�d defmacrol   hd optsjd niljd falsehhd deleteab  d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   aDispatches to the path only if the request is a DELETE request.
See `match/3` for more examples.
hhd forwardab  +d defmacrol   hd pathjd nilhd optionsjd niljm  �Forwards requests to another Plug. The `path_info` of the forwarded
connection will exclude the portion of the path specified in the
call to `forward`.

## Options

`forward` accepts the following options:

  * `:to` - a Plug the requests will be forwarded to.
  * `:host` - a string representing the host or subdomain, exactly like in
    `match/3`.

All remaining options are passed to the target plug.

## Examples

    forward "/users", to: UserRouter

Assuming the above code, a request to `/users/sign_in` will be forwarded to
the `UserRouter` plug, which will receive what it will see as a request to
`/sign_in`.

Some other examples:

    forward "/foo/bar", to: :foo_bar_plug, host: "foobar."
    forward "/api", to: ApiRouter, plug_specific_option: true
hhd getaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   ^Dispatches to the path only if the request is a GET request.
See `match/3` for more examples.
hhd matchaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm  �Main API to define routes.

It accepts an expression representing the path and many options
allowing the match to be configured.

## Examples

    match "/foo/bar", via: :get do
      send_resp(conn, 200, "hello world")
    end

## Options

`match/3` and the other route macros accept the following options:

  * `:host` - the host which the route should match. Defaults to `nil`,
    meaning no host match, but can be a string like "example.com" or a
    string ending with ".", like "subdomain." for a subdomain match.

  * `:via` - matches the route against some specific HTTP method (specified as
    an atom, like `:get` or `:put`.

  * `:do` - contains the implementation to be invoked in case
    the route matches.

hhd optionsab  d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   cDispatches to the path only if the request is an OPTIONS request.
See `match/3` for more examples.
hhd patchaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   `Dispatches to the path only if the request is a PATCH request.
See `match/3` for more examples.
hhd postaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   _Dispatches to the path only if the request is a POST request.
See `match/3` for more examples.
hhd putaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   ^Dispatches to the path only if the request is a PUT request.
See `match/3` for more examples.
jhd 	moduledocham  �A DSL to define a routing algorithm that works with Plug.

It provides a set of macros to generate routes. For example:

    defmodule AppRouter do
      use Plug.Router

      plug :match
      plug :dispatch

      get "/hello" do
        send_resp(conn, 200, "world")
      end

      match _ do
        send_resp(conn, 404, "oops")
      end
    end

Each route needs to return a connection, as per the Plug spec.
A catch-all `match` is recommended to be defined as in the example
above, otherwise routing fails with a function clause error.

The router is itself a plug, which means it can be invoked as:

    AppRouter.call(conn, AppRouter.init([]))

Notice the router contains a plug pipeline and by default it requires
two plugs: `match` and `dispatch`. `match` is responsible for
finding a matching route which is then forwarded to `dispatch`.
This means users can easily hook into the router mechanism and add
behaviour before match, before dispatch or after both.

To specify private options on `match` that can be used by plugs 
before `dispatch` pass an option with key `:private` containing a map.
Example:

    get "/hello", private: %{an_option: :a_value} do
      send_resp(conn, 200, "world")
    end

These options are assigned to `:private` in the call's `Plug.Conn`.

## Routes

    get "/hello" do
      send_resp(conn, 200, "world")
    end

In the example above, a request will only match if it is a `GET` request and
the route is "/hello". The supported HTTP methods are `get`, `post`, `put`,
`patch`, `delete` and `options`.

A route can also specify parameters which will then be
available in the function body:

    get "/hello/:name" do
      send_resp(conn, 200, "hello #{name}")
    end

Routes allow for globbing which will match the remaining parts
of a route and can be available as a parameter in the function
body. Also note that a glob can't be followed by other segments:

    get "/hello/*_rest" do
      send_resp(conn, 200, "matches all routes starting with /hello")
    end

    get "/hello/*glob" do
      send_resp(conn, 200, "route after /hello: #{inspect glob}")
    end

Finally, a general `match` function is also supported:

    match "/hello" do
      send_resp(conn, 200, "world")
    end

A `match` will match any route regardless of the HTTP method.
Check `match/3` for more information on how route compilation
works and a list of supported options.

## Error handling

In case something goes wrong in a request, the router by default
will crash, without returning any response to the client. This
behaviour can be configured in two ways, by using two different
modules:

* `Plug.ErrorHandler` - allows the developer to customize exactly
  which page is sent to the client via the `handle_errors/2` function;

* `Plug.Debugger` - automatically shows debugging and request information
  about the failure. This module is recommended to be used only in a
  development environment.

Here is an example of how both modules could be used in an application:

    defmodule AppRouter do
      use Plug.Router

      if Mix.env == :dev do
        use Plug.Debugger
      end

      use Plug.ErrorHandler

      plug :match
      plug :dispatch

      get "/hello" do
        send_resp(conn, 200, "world")
      end

      defp handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
        send_resp(conn, conn.status, "Something went wrong")
      end
    end

## Routes compilation

All routes are compiled to a match function that receives
three arguments: the method, the request path split on `/`
and the connection. Consider this example:

    match "/foo/bar", via: :get do
      send_resp(conn, 200, "hello world")
    end

It is compiled to:

    defp match("GET", ["foo", "bar"], conn) do
      send_resp(conn, 200, "hello world")
    end

This opens up a few possibilities. First, guards can be given
to `match`:

    match "/foo/:bar" when size(bar) <= 3, via: :get do
      send_resp(conn, 200, "hello world")
    end

Second, a list of split paths (which is the compiled result) is
also allowed:

    match ["foo", bar], via: :get do
      send_resp(conn, 200, "hello world")
    end

After a match is found, the block given as `do/end` is stored
as a function in the connection. This function is then retrieved
and invoked in the `dispatch` plug.

## Options

When used, the following options are accepted by `Plug.Router`:

  * `:log_on_halt` - accepts the level to log whenever the request is halted
jAtom  �   @Elixir.Plug.Router__info__	functionsmacrosmodule	MACRO-get	MACRO-putputMACRO-optionsMACRO-matchnilextract_path_MACRO-forward=	__block__
MACRO-postpostMACRO-deleteMACRO-patchpatchcompiledoElixir.AccessgetfalseElixir.KeywordpopElixir.ArgumentError	exceptionerlangerrorElixir.MacroescapeMACRO-__using__useextract_private_mergerprivate&	update_inMACRO-__before_compile__build_methodsElixir.Plug.Router.Utilsnormalize_methodmake_funElixir.Enummapinjoin_guardstrueand	__route__viaElixir.Listwrapbuild_path_matchhostbuild_host_matchextract_path_and_guardswhenoptionsdeletemodule_infoget_module_info Code  U          �   K   � " 0e;e`25BERU0@G @@GP@`Jp�b0�@3@@�� r@�@�@��0�0�@3@BЙ@�@�@�@�P�9:0B B #+�0#@G � r0@3@��`�050EEG0E#EG@F0#G�GGE#GPF0#G�GGE#F0G
GG�p
@@
@��
0@3@F��
@@
@��
@@@@4@$@
@3@#��  ,�,
0F GG@= @
@��  ,�,
@
@�� = @G`�� ��0 9":" B B#@$@#$��=9!:! B B@Gp� @@#@Gp@@#� @6E4EG�E#EG0##E$3EG@33ECEG�CCESEG�SSF0cG�GGSEcG�SF0cG�GGCEcSCF0SG�GG3ESC3F0CG�GG#EC3#F03G�GGE3#F0G
GG@!��H"�H#�
# $ EEG�F0G
$GG�GEG�EG�EGF0G
GGG%�@�0&@3@�'�
%(7' @
&� P,)�+*
)@� *�`EEGF0GGGGEF0G
'GGEEGF0G
(GGGEEGF0G�GG +�
) ,@G-�
* .8/A#34030@#@�p0F GG@/400 F GGG0  @@@
,@#@
+�0�@@�� ��EEG�#F0G
0GGG#@� � 40F GG�G@ 1��
02@3@3�
1 4+5
25� EEF0G
3GGG6�
4@7@@@#@3$@4,8�+9
8@
5@3�  9��@� .9;:; B B@4@4��9::: B@$� (@#@
9@$@#$�!  �!�pF`GGG4GGGG$@@:�H;�H<�"
;=9>:>0B B #+>
<8>#A#3C8>CACSc4>c`@3@S�"0F GG@> �#0F GG
2@ ?�b@@@
@A�0�@B@
=@C�p
0D@3@E��
@F@
>@G� 
? H@� N�I� 
?J@@� N �   StrT    ImpT   �                                           !   "               !   "      +   ,         -      .   /      6   7      +   8      +   :         @         @   ExpT         ?      J   ?       H         F         D   	      B         @   4      7         2   )      ,   
      &   #      $                                                
         	               
                  LitT  @  "kx��Y[k�F�e/N�@J�mJ����6�m�C
%�g�]�jG�4�H��'=􏔘R��;�ҟ�_�33��W��Tc�f���oΌE�LQ�O��K�R�%q��8��_>��M=4�Z��9��5Mb�ΞGrQ�Lm�Agc ?5�%�tEk#�b��|)ަcAI�?3���	�h��f�0 ���ęT6ҙ�����g�/a8I�:��',?r������}�������L��e{���չ�f.���ލ�;�%�B�i�|��Y����Y�,a|��b3D�a�A�W�a�!�U`���j9�3$p
&�B-E����4�NWK���q+&���Qc��2Ȁ%��(>1#�35�Ḧ́�9�#t�V� �Ba~?�����B����ph��퇣Z��"�_�(n��n��ɳH�[?�&Q�A�H���{ȏ�(%�Pm�04mĀ� �h!k�""�����O8�	�|t�tU��ї�xɦ8n��m��޵y��Tt��m�F�fP����Am�L�w�.�)�P��W=fN��2a4}f�d�4�"��b*ӈ1�3��E�eB��]xq��?1i��0 ~�.�;����ʺ%�;�,���^$;��I�LV�L6>A H�L<?��kԱ�kL�BD4Fl�	o�F���*m��Uv�v_Y#7#�yٚ�Ei��o45|g`D}w*�vU9��Z=U��.j�U�![M��E+[Q���O���8֌u1.��ʋ�*�a,�����%�b��W�Y�(v�Y5�ʛ�*���Y	IE�2ͪB{�۪bm7��t�^΃�"w�i�g�Y�f)�p��)ʋo����WBx��7߲P�㮓����d�{�~�]��<t�l��r��!:�������c����OD�ȱo�@E���1�DB���`��UI���^3�!y�5�_��F�~70"�PI�Wk�ms��{�Vʴ�}�2�N ���nH�*?V���c7[�ț�*�Q��k��/H�0�%�,���N%!��8SK��G�_tω�Z�T�>
���M��-�H�]]n��a�b��_�P_Jz�z��i{�	|ݰYeb���/Z))1fb�yK����U��m�a�6���k6I֫�+�V�]�eUw?�*>���ê���Aݪ��s�4��#���j��|�� J�e�\$��u?2�֬�������O}�w��]�s����A[2 ���v��R����מ�2IQ�mq]-���"x�u"����=�7α����S��3q
iCQ��@�ܼ������@�_&u�5����߶�A����t�z@|�\X����[�� o��LocT   L      ;      =   1      4   *      .   %      (                  Attr   (�l   hd vsnl   n G�-Y�lA��U�\��%jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek </Users/davidwalker/erlang/rumbl/deps/plug/lib/plug/router.exjAbst  n�P  �9x��Mo��u�\�lKqҤhs*��!n��Ǡ��ז�6�#�@P���$�wI��Jr� N
$��W-�h�^�_�|�o�M>t>�伙�7w%Q�`�μ�������FSo��j[���|W����A����FKo��|�5�#��zc�gs`�Ӗ�j#�V͡c�>�uNo��P����ƛ�{���ϻ6��=glksb����G�W���rn���6�1ZL�[���?�����Wծ�f��Щ�Z�����gZ���%ڪAUĦnz�6!�[���n'iۺ��1�����6h��µߴ-����XX� ˉ`�
Zl/ �mh1:#H1���~BUMk�F��#?�*���՗(�s��F���A4���9���6�pk�.8.�u��Ϗ,�8n_�PP���C�~*����=�Fj��IDI�H����C���|1�~?�^�!���'��9#E�����H�c��_rJh���@y�m)Ґ�+�~?"��M��;R���&�n^0-�X7\����b���OA�7/�7E�战��a$��a�i�� 4�P�X�Rq�*���y�c���#� �� B8%e �O��ˆ�����������\a(��O�$����D$^�M(!�E����8 \l߅����[�һ|����p����݈R��N'��3Z�Yz����<�]���͛WW��62�3x#l=�L�7,ߓp�2��e�M��F`����"�!���"J�4���V��	��1)��{�O~��@�;2��Bߑ
}'Y�O#�>M�8b&��TD�T��i�Ɵ�5��M�Y$Mؚ���F��MDΩc�h�a�3��hO�_m��>���4k�n=�	B�/��J״��1���TQ�3D"�ᱍ�(�w��#BA8tcM��_�>'�o�߆jb�K~L
Hb�}QU�}I���0�gŕ�,�����9'Z#?�3�����M53�����˳�.���P[�~�������E�~(��,�s�K�0y̸��3��H�LZ3Q�Ô�
�Y��i�����k.^��F��M�W��ܢ�jS��=�2��Mk��3��D2Y6�m�hk"Lf;l��0�oӷ��T�)6Z��D�Kx�V�YjK��������P���K�8���|�AY�ě�Z���:o�y=2a�-8�`�^ .�e��i��㩍�-��_�'�"��׵�x�W;��YDj4��\S���w5�3j'��Oگ]q�Gx����]NkC��urd�gg�]c�1z������Ϡ����:C�:c���E�Ƈl2��f�X�kgYo��5��P/�z|8ƪ�PJc��@*^$Pg\�lZe3��+�\3P�B�L$����vfZv�.��>�����J|=V�6̟���|&��vTu&]��/^i�����bFkSs뷼���	��j*9G&���_J��ΤX>�+���,d�}���=��U8�݃g�{�҂=�tOz���\Z�<��҂�f� }.b.��<��@J�e'�J��$�q��W_K��?
߇H�e
�E�/�~��_Ej}���F�@}%�%E�*U�DhEY�@��C�l�B��]�U$,�A��EC�ˊ4@�� |][���0�.�N���B�����q���[\��-i(�����*ߚ�e>M���xn���*,����	N ~A�t��ڰ�y�ϫ��'�Jyɲ��p�i ^���}�9��s*t����*��լ[���*o��
	Gb�(WتTa���W)�$�e�J�6'��`��pڧ����n��P]��D����K��)�w8:���ERt�m��ۼ�~>6��ˁ>il��%~���������6�iXg�60!$m8������3i�j=�>BY�_ �π�P�_��*2y�Jc�sHJ�����t�+_��Wv��VQVE�d�Q.'��L��65�2�l�[i\�� �� ����(��v*��9������c������h`�;2Jd2m�V �.'�nF�v��ۍRԃ��C�<�ү���M��ma��Rå����7�
���4��?��v�!0"ᨱ:�$%�G�<���<�9��]�`�K���m��6k\��n�E����p�ɕ �@��.���t��6�M�eT�?O�0�T&^B(��Aj���>�-65_tF�*��P��a��S�ؖ���5� ���?��:�E%t:]�˻�Z�X���w��������v$��6�a �g
�Y��ނ��;%�ܘs*�������檔�}&^�v���W�k~&���(�!�i��M�|���ߓ0�LO�k�j��Y��a�:
De��#�C����G��?9T41�Y����I�`$��>̼'��|�5���9U�	:SNU�uPyN┑~�p��3�M������X��U�]�1A݄53��N���#"�����&����aJ���h��OjN��o�r�)��u�|ɂ�᯳�9�x�vR������^��Y�eNUeNʧ+�3$Nm��ٺq,��	�ҧ_3ߝ�`i���b�Z{Zj�۱;��om�f������OD?r�K5f��9���-���L�DB��;�?���X"�4�3�����"
{�MLi�lm\m2�\R+�Yom��U�*��/O�m��Em3�����q��c�\�[�����Jz\mR���?������j9o�y4e2��`�}�ӈz{�p]S׺����GrG5sV2_�Ͱ��N`�9+�H۱Oy�^|/|'�����D,pXq��_�_�7���G.��$�O$����ɋ��E�ChI�;��h��L�OA)�c����\�m�R �*w{��Qi���p����/��boT����-�e� �qX�(_��ʥ.I�����Rg%\#��7EKى.�NN��/ڛ��y'G��]Ĝĝa���f�OL����L�~-R�%�<	��n�Ik\q	�	��Ⴏ����뀻|A�q�H��!�Io�=��^E ���'0���M4����fƛh�^�r�.09�kx3'ض5��o$)���]��J���9(�$�E1��86�-�^���9�P�t�� 5G�}�Jp<��R|�G*[lx�ŀ���EV��g��x�@o�a���?�@%Bdx������B~��0���K_��`J�����sH�!'�����oDX! ����އ���d�C,C���"g2]Y���)~��_�����K��'�|B�,t�q��+�W�b����A���i1�f�xp��攌hu���X9h�����OV�Lc��ԇ��U�~U���H�։�⿆�F' ~Q�� ¯����ڎ�\�bf؛����a����g�v�ҏ�?DR���$Y�����/�]V���vM�.�5�M��n�`,�06v}�Qq�o�D���>�S�7ou����M��yȭ-�ΐp����Ou/�$m�td5q˥��Q�qT��aɂKxx���Չ��R6MMH��DwQz�_�1Vt����'��u1T�o��q3��?�u���i)�5e�b��;�Qx'���׍�� �ؽ]����2�q-�q�
�_��\@Nˡ�"4��&cJ0���]�5^��B��Ep��@;y˽Ɛs3&pތL�k�Z�@�	+lg6G$�Σ�G��� q�z�&{r���Y����G�|�!���y4�r�0i��Wp�G�w�P D�����,4>4>N��1��O긌x� �M�OH�݅��lkW�(X�e���u�E�>��%�Dc��D�Ϥȟ�>�����E0Z�j-
�֢D*A�$�Z�{\w1Q<��0��$!��"1���,�ѳ�(1�nq-�W�Zj/�"��RJ��ӖZ�"X�c�T�J�ԕT+h+h%YA����oI�iő�⍠��50  Line   o           8   #   	�	�)	�)�)+	�)	�)Q)T)V)W)Y)\)a)b)R	�)m)n)p	�)w)|)�)�)�)G)H)I)J)K)�)� lib/plug/router.ex 