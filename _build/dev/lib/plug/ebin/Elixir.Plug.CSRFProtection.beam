FOR1  (�BEAMExDc  
��hd elixir_docs_v1l   hd docsl   hhd callaa}d defl   hd connjd nilhd optsjd niljd nilhhd delete_csrf_tokena aod defjm   xDeletes the CSRF token from the process dictionary.

This will force the token to be deleted once the response is sent.
hhd get_csrf_tokena a`d defjm   hGets the CSRF token.

Generates a token and stores it in the process
dictionary if one does not exists.
hhd initaa{d defl   hd optsjd niljd niljhd 	moduledocham  �Plug to protect from cross-site request forgery.

For this plug to work, it expects a session to have been
previously fetched. It will then compare the plug stored
in the session with the one sent by the request to determine
the validity of the request. For an invaid request the action
taken is based on the `:with` option.

The token may be sent by the request either via the params
with key "_csrf_token" or a header with name "x-csrf-token".

GET requests are not protected, as they should not have any
side-effect or change your application state. JavaScript
requests are an exception: by using a script tag, external
websites can embed server-side generated JavaScript, which
can leak information. For this reason, this plug also forbids
any GET JavaScript request that is not XHR (or AJAX).

## Token generation

This plug won't generate tokens automatically. Instead,
tokens will be generated only when required by calling
`Plug.CSRFProtection.get_csrf_token/0`. The token is then
stored in the process dictionary to be set in the request.

One may wonder: why the process dictionary?

The CSRF token is usually generated inside forms which may
be isolated from the connection. Storing them in the process
dictionary allows them to be generated as a side-effect,
becoming one of those rare situations where using the process
dictionary is useful.

## Options

  * `:with` - should be one of `:exception` or `:clear_session`. Defaults to
  `:exception`.
    * `:exception` -  for invalid requests, this plug will raise
    `Plug.CSRFProtection.InvalidCSRFTokenError`.
    * `:clear_session` -  for invalid requests, this plug will set an empty
    session for only this request. Also any changes to the session during this
    request will be ignored.

## Disabling

You may disable this plug by doing
`Plug.Conn.put_private(:plug_skip_csrf_protection, true)`. This was made
available for disabling `Plug.CSRFProtection` in tests and not for dynamically
skipping `Plug.CSRFProtection` in production code. If you want specific routes to
skip `Plug.CSRFProtection`, then use a different stack of plugs for that route that
does not include `Plug.CSRFProtection`.

## Examples

    plug Plug.Session, ...
    plug :fetch_session
    plug Plug.CSRFProtection

j   Atom  P   QElixir.Plug.CSRFProtection__info__	functionsmodulemacros"ensure_same_origin_and_csrf_token!nilfalse9Elixir.Plug.CSRFProtection.InvalidCrossOriginRequestError	exceptionerlangerrorskip_csrf_protection?
__struct__privateElixir.Plug.Connplug_skip_csrf_protectiontrueget_csrf_from_sessionget_session	byte_sizedelete_csrf_tokenplug_unmasked_csrf_tokenElixir.Processdeleteplug_masked_csrf_tokenvalid_csrf_token?Elixir.Basedecode64okElixir.Plug.Cryptomasked_compareget_csrf_tokengetputxhr?get_req_headerElixir.Enummember?js_content_type?get_resp_headerany?cross_origin_js?methodnotbadargverified_request?termparamsElixir.AccessElixir.Listfirstunmasked_csrf_tokenensure_csrf_tokenput_sessiongenerate_tokencryptostrong_rand_bytesencode64maskbit_sizeallinitcallwithElixir.Keywordclear_sessionconfigure_session0Elixir.Plug.CSRFProtection.InvalidCSRFTokenErrorElixir.KernelinspectElixir.String.Chars	to_stringElixir.ArgumentErrorregister_before_sendmodule_infoget_module_info-call/2-fun-0--js_content_type?/1-fun-0-Elixir.Stringstarts_with?Code  �          �   Z   � " 0e;e`25BERU0@G @@P@`Up�b �  @@� ",�r,��@�0 �0�@@ > ��@������@�#�+�#
���� 
3+�3
@
��� �+�
@�ЙP
�@G�`  @,�r,���p|0+	=�,r+�@r@��
   @
��@@
��@ ��
 t  w 	� #y t0 3w3@	 � Cw3P	� Sy3  `@C@#@S��P9: B B#+
@#@#@��0` +�@� @���J��
!  @
��p,r+�� :�C@@
@� �@�
$ @G � �@G0� � �
( @G@� �@g @@� �!�
+"�!�(@�#
,+(#
+(GP@��0);)@�#
$#@��
�=%$@�='%0*;*@
&�'&@ '(�! �+!
@�)@
.�*0F G
.G@�+�
/ ,  @@�.�- 
,=/-�.��G` 
0�.@
,�p /@@Gp� �,0r+4�0�2�1 
1=31�2��G�  
0�2@
1@�p 3@G� �@@� 4,5r+6�5@G�@� ���@@� 6,7r+8�7@� 8 9�
5 : @
�p,;r+<�;� A@@
@�  �@<=�!
6 >  @@
@�"@@
�#@+?@ ?@G@#@�$0 @�%
8 A  @	�&�& B�'
<C@�( A@@@�) �)�)@| #o#�  � Z
>� @D�*
?EF�+
@ G0 @@$�,�@@
@�- �@@$�. ,0Q;Q@
H�IH@$=PI@
A@�#@�/00L;L@
CJ�KJ@G�@$$�0 �0=OK$@�1�1L$�25M=NM�2N�2@| 0#o#o	@m  \@ Z
>� @�2�2O@P@@$g@@$�3 0Q@
.�.R� 
L S@� NT� 
LU@@� N V�3
N W �X�
OY@G��N   StrT   @option :with should be one of :exception or :clear_session, got ImpT  �       	   
                                                                "         #         %      &   '         )      &   *         -      2   "      3   4         7      9   :         ;         <         =      B   "         D         C      E   
      F   G      H   I      J   
         K         M         M      P   Q   ExpT   X      L      U   L       S   @      G   ?      E   !                          FunT   4      O      Y        ���   N      W      ���LitT  $  �x���=O�0��$*JK����ؑ�((�l�e�S�1ε$s�8���+�}<w�;���!�S�&y$HR,��X��k�N�Z�f����L���z��3�JgwZ\:	�I-|��F��ܱ�P�=>L��wّ����Sl�|?��6[87�|c,���p0(+M���9��vG�F����3h3k++H��V�~	�W�:2�~A2ͮo���ߛ?/��O�ſ�5̲���h�z�ҭ`gᘞ׶/����3�_p��7Bh�`Vs+z6a�(ə�k�T|0
��LocT   �      O      Y   N      W   <      C   8       A   6      >   5       :   /      ,   +      "   (          $                                          Attr   J�l   hd vsnl   n :�M)o �r�w;�vjhd 	behaviourl   d Elixir.Plugjj  CInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek E/Users/davidwalker/erlang/rumbl/deps/plug/lib/plug/csrf_protection.exj   Abst  q�P  ;�x��[�o�όM��3�l6���9@p�&ABfCHv��,Z8��[�����LwS�c� @v�/���%>r���d�H �	����)IUOwW�����3�U�z�W�W���^�;:i�� $�Z3���B�G��]EaH��v�Q�.l�x-�ib����,�=�i�	��Cv�\������O���^�ٮ���j��q%t<w�EC����g����i:��s���s�ا<�UG�N�:�q�6�c�@�|<�,f3�өϲ��Y5�~-WP�����.7q����Л�.*���5*�e�uB4��x"�q�^�t1������+U�.k-��-�av�����.uk�R/�vz-��V�= �I�`c���(�$�$�k�8�B��mQ��p��0ص�t݌ö�Ĺ$��;׈�����Ʉ�� �Y��J����t2��7�B�����z#�`d%ֲ�qC\�P��{�
?IV!�,	���\NAO�]r�&����.�+CJ}����D��D����~H�)� c a�]�9v�6�V�Z��Ss\�����bO�`���ߑ���纇��M[��C�z�c㩈|7�<n؅���AbK#������%
f�P�����P:���GB<��ZJ��3 ������F��C��/���[5�@&M��xۗT=&�#�e���=�3��؇Yh>�"�)>��Ax�� jG�l�a̠��i�<r��I)$�!�D�Ma7�I�~j~L;�%�����<��o!��5��ժ:�i[tP	�S�3���b8r(��������XHڸ�C�[�E7E4�Zh�	�J]��2V#x���3󼞽A;�%#y�F2�U⵬ {2�P���.p� ˉ�\�>�PX�����e	a���*ZΞ'J危i��C�/C�O9n�����W�&�v��P';n�cª���q��!��P�r:i�H��
Q.Kķ܇(��W�X�m8��X�E����<V�X]�f͋l>G�2��7U]��F�[��������y"���|����*K���M�5����/͇;�d��B��=m�ߍض��P0��ݍ��N�:O��y"C�;�NU����KQv:M�6] M�9a0v��g��i|.��%6O�3a��AR�WR�@�T�*�ZD��%�ْF���[���U��b��]q���!��U�W<�����45uQ_K�Eo:�xJ�h(��k��׀��lB����ϩ��	����%�Y��$�A���i2u�����@�0�}a�YZۢ' ��	�i*
�|� 		��v 
G%�#1R(EPA%jU~~P� D�1[#%� )-�>�:O1�����(;����ͅ��e6oi�o��Z�k,�����P]��X�3�|F���*6f�+I�E-�
���6�4�x��k�����ɤfK|�s߰%�>} ��H�������­)L&��!*c�� ^����٘@�x��]���ݤwd���NX�o�������0���^��;����6Mwnh�R�.nw`���vOt��q��>�sr��r���]z(�-�Q�����5��n棌�Q��yY�8�]�8�R�Ht On�=p,��rk"��$�{�H�[�!��6�
*������A��t*�A��P͑q����i�-k��c*\K�k���<��pX��A�Z�ӑ�F��Q����MY�q��EU#צ���5�Ӧ������[����-nAN�Q����QH��a�鶹�t�;�2]b�0L�ն�\1�)~_���Z���-~[�U���?v�sWDZ�A�+�D�)>(p��?�i��\x���哣�?��/��0U���ѿ4H+i�zY���+ "/�ZzyF*�$�f�b/a��"+��ks�����48�p�UpYYr
�"	Ԯeھ#�Gx������m���}��E�"J�xN��C�!&-�o�Ÿ��:�"	H���c�Av��cG:v�� ��5�t�ީ�+�A����i�9پ�_�g�?�T��*:f�����V ��W
U$��0Jz��䕎f5wT![�Y�:�蘩
�RE''U@Q&��dQ�B�h���*�o�Ÿ��:��t��f�[;-��׃{�����c7��kqu��JD�5W�����N����5WWahN6|;A�W"�>�'(���Sw�n�?���xw&n}�����
;�F�Ϊ�U	�X#p��7 g�wZ�w�[5������쯴Y>�|�/�jJ�_+������Е�7D\n ˵��tn~���a�aC�QǱ�Pа!EwCSѐ���x�2�RW<Ʌ�����	
�����W����6ō^Nm}�+���PW#�+�s�-��'�,�Pw�Hs���z��y�������G�|�h���{̕?�?Χ��^�++�+ׯ��yZL�<���؅�J��R4g�'�l��MrT��x��$*���ߘ&���eikX���Eo�������T)v���yd�T���T�.�ϭY��e7g�4������T�۽W����!���[f�i��&A:����L⼞B�y�Z������fg��|ۿn󗦜��5��.���0��ȸ���o�����@��)�S,��a�2��J?�8O%����U�>-),�9��:cܚ��18c��'rFW/?+��l>�>���|V�������M��-�	��J�u�p��y���V%[̛�����~Oӄڤ��}��.�r��-�����i�Kb$���ϝ�p�a�oeK@O��>���;�����NP�.�s��� ��՚s�;�2~��9m=��!]?�}u\��?�)o�41"�rT�}^4�|��yY�����Ԣ^�.�����G�W�F��K�/���9d��)�S2�D��Zg���El2d� �͒Z�%#	�*4�ܽ��݋���?���xP��M{|
�{.�W?�J����_�׼p\���h���<Sߖ2qq̙��/�B�E!L2��5è�N	>I��_&���6��oc�c��N`����J�^�c�yL�ѷ�Ս=��^�g���W�yK�q��D�����]ל �Ě�U/b	׆�S"� ��yf	��tf	���aЬ事`�rI������s   Line   �           V   3   	�	�	�	�	�	�	�	o	p	q	�	�	�	`	a	d	e	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	{	}	~		�	�	�	�	�	� lib/plug/csrf_protection.ex