FOR1  1$BEAMExDc  .�hd elixir_docs_v1l   hd docsl   hhd __before_compile__aad defmacrol   hd envjd niljd falsehhd 	__using__aagd defmacrol   hd optsjd niljd falsehhd compileaa�d defl   hd envjd nilhd pipelinejd nilhd builder_optsjd niljm  Compiles a plug pipeline.

Each element of the plug pipeline (according to the type signature of this
function) has the form:

    {plug_name, options, guards}

Note that this function expects a reversed pipeline (with the last plug that
has to be called coming first in the pipeline).

The function returns a tuple with the first element being a quoted reference
to the connection and the second element being the compiled quoted pipeline.

## Examples

    Plug.Builder.compile(env, [
      {Plug.Logger, [], true}, # no guards, as added by the Plug.Builder.plug/2 macro
      {Plug.Head, [], quote(do: a when is_binary(a))}
    ], [])

hhd plugaa�d defmacrol   hd plugjd nilhd \\jl   hd optsjd niljjjm  VA macro that stores a new plug. `opts` will be passed unchanged to the new
plug.

This macro doesn't add any guards when adding the new plug to the pipeline;
for more information about adding plugs with guards see `compile/1`.

## Examples

    plug Plug.Logger               # plug module
    plug :foo, some_options: true  # plug function

jhd 	moduledocham  
�Conveniences for building plugs.

This module can be `use`-d into a module in order to build
a plug pipeline:

    defmodule MyApp do
      use Plug.Builder

      plug Plug.Logger
      plug :hello, upper: true

      # A function from another module can be plugged too, provided it's
      # imported into the current module first.
      import AnotherModule, only: [interesting_plug: 2]
      plug :interesting_plug

      def hello(conn, opts) do
        body = if opts[:upper], do: "WORLD", else: "world"
        send_resp(conn, 200, body)
      end
    end

Multiple plugs can be defined with the `plug/2` macro, forming a pipeline.
The plugs in the pipeline will be executed in the order they've been added
through the `plug/2` macro. In the example above, `Plug.Logger` will be
called first and then the `:hello` function plug will be called on the
resulting connection.

`Plug.Builder` also imports the `Plug.Conn` module, making functions like
`send_resp/3` available.

## Options

When used, the following options are accepted by `Plug.Builder`:

  * `:log_on_halt` - accepts the level to log whenever the request is halted

## Plug behaviour

Internally, `Plug.Builder` implements the `Plug` behaviour, which means both
the `init/1` and `call/2` functions are defined.

By implementing the Plug API, `Plug.Builder` guarantees this module is a plug
and can be handed to a web server or used as part of another pipeline.

## Overriding the default Plug API functions

Both the `init/1` and `call/2` functions defined by `Plug.Builder` can be
manually overridden. For example, the `init/1` function provided by
`Plug.Builder` returns the options that it receives as an argument, but its
behaviour can be customized:

    defmodule PlugWithCustomOptions do
      use Plug.Builder
      plug Plug.Logger

      def init(opts) do
        opts
      end
    end

The `call/2` function that `Plug.Builder` provides is used internally to
execute all the plugs listed using the `plug` macro, so overriding the
`call/2` function generally implies using `super` in order to still call the
plug chain:

    defmodule PlugWithCustomCall do
      use Plug.Builder
      plug Plug.Logger
      plug Plug.Head

      def call(conn, _opts) do
        super(conn, opts) # calls Plug.Logger and Plug.Head
        assign(conn, :called_all_plugs, true)
      end
    end


## Halting a plug pipeline

A plug pipeline can be halted with `Plug.Conn.halt/1`. The builder will
prevent further plugs downstream from being invoked and return the current
connection. In the following example, the `Plug.Logger` plug never gets
called:

    defmodule PlugUsingHalt do
      use Plug.Builder

      plug :stopper
      plug Plug.Logger

      def stopper(conn, _opts) do
        halt(conn)
      end
    end
j  Atom  �   AElixir.Plug.Builder__info__	functionsmacrosmoduleinit_fun_plugfunctionlog_haltlog_on_haltElixir.AccessgetnilfalsetermerlangerrorElixir.KernelinspectElixir.String.Chars	to_stringbit_sizeallelixir_quotedot	__block__MACRO-__using__plug_builder_opts@compile_guardstruewhen->docasequote_plug_callElixir.Macroescape.
MACRO-plug{}plugsMACRO-__before_compile__Elixir.Moduleget_attribute	byte_sizeElixir.RuntimeError	exceptioncompileplug_builder_calldefp
quote_plugraiseinit_module_pluginitcallfunction_exported?messageElixir.ArgumentError	init_plugatom_to_listElixir.Enumreducemodule_infoget_module_info-compile/3-fun-0-   Code  	          �   V   � " 0e;e`25BERU0@G @@GP@`Sp�b0�P0F@3GrGGG#@3�� �@�@@@4@$@�@3@#�0  ,��+���@�@�@04;4@r�RМ��� R=����@�G   ��@�@R@4�@p 4�@ 5=�@0@4@$$�@ 5=�@0�@@	h| @#o#� �4 \� Z
� \ �@=�� R=��P�G   ��P@R@4�Pp 4�P 5=�P0@4@$$�P 5=�P0�P@	�| @#o#� �4 \� Z
� \p�@ E3@#@G0@C�@ @�`PP�EEG@F0G
GG �pJ4��
   EF0G
GGEF0G
GGPGEG`EGpF0G
GG!��
 "+#
#! EE
F0#G
GGEE##E#F0G
 GGEG�F G
!GEEE
F0G
"GG$��
#0%0$;$@R&r'&0@#@��`�EG�F0#G
&GGEEG�F0G#GG'0@#@��`�EEG�F0GGG(��
' )@#0+*��
'0+0E#G�EF0G
(GGEF0G
)GGEF0G
GGPG,��
* -0 @$�/�. R=0.�/��G   ���/@@R��p 0@
)�� p@�2$�1$ R=31�2$��G   �$�2@R@$�p 3@
� p@+9�5$�4$ R=64�5$��G   �$�5@R@$�p 6� 57=87�08�@| �#o#o	m  \Z
� @���9@@#@$�0 �0�9::: B B#EG�F G
!G#EEF0#G
1GG�GE#F0G
2GGPG :�H;�
3@<9;:;@p@B CBSB cB0@d@#4@S@c#@C@3$@D@T�0%@0DT;TD@r=R@=5>D@D=?>@D�0?�@| �#o#o	!m  \�2Z
� \;@=C@@D� 5A=BA�0B�@| �#o#o	&m  \�2Z
� \S@C��I   � \Dp@#@@@#�`� "@C@$#@4@3@D@CD@4�00�@�7EG�F0G
GGEEG�E$EGE#F03G
4GGPG#E3#EG�##F03G
 GG#E3#F03G
 GGE3#F0#G
 GGE#F G
!GEEEF0G
"GG0D�JTE�
50F00@#@
6#@C@@C@$�p@@
7@!#@$�0�,G�+J�G@$$� 5H=IH�0I��	�P� \�F #G
9GE#���JP F@GRG$GG0K�
;L9K:K00B BB @@$� �8MA#+M	E8M#A#3C+M3	l8MCACSc+MS	i8McAcs�+Ms	x8M�A���+M�	i8M�A���+M�	r8M�A���+M�	.@@#@$0F0M@@#@$0�0N�!
00O0@@@#g @G�@#@� �"0�0F GG�G@ P� 
? Q@� N�R� 
?S@@� N T�"
A@U0@@#@3@$�"L@#@3@$@<0  StrT   � halted in /2.call/2no plugs have been defined in expected /2 to return a Plug.Conn.call/2 to return a Plug.Conn, all plugs must receive a connection (conn) and return a connection plug must implement call/2 ImpT   �      
                                                      $   %      +   ,         -      .   /         0         8      :   /         <      =   >         @         @   ExpT   d      ?      S   ?       Q   0      O   *      -   '      +   '      )                   FunT         A      U       -�LitT  C  	�x��U�n�@];&V�Z�V| �@�\@�4\�
.�W��Y����JTq��Џ��];v�*M��\����{ofgƄ�#B��R�ӛ�����fB�%���������Z[Pʜ��E.�x�d�����E�(>{�P
�2#TJ)�] '��渊��{1�b&���C���������}9A���)�!��������������*�A[)���~B�r䩁��䤦���g��+��H-�K����Z��4S�L�$�Nh��8�; � /�0[�#P�ɓ{���j�q�$AEL�m���T��ѹu�G��:�VOGDU�1c����$t�<��qeQ7���xk�'k �g���&iWR7���u*��6-w眕hɼ���v\\��}�!��ja�h1*$���㬼�z��rH��@d+)o�Y�B2�ȭ��}�\\%��!s�a[�vz�)�W�7�	���m�;�������}�"YX�\o�t�rX.|܈���r�㊛����V���B����m�foZ�{#A��%�m'x��i�j�?��ͯ�)�&��W�zc����� *�� LocT   d      A      U   ;      L   5      F   3      <   #      %         "         
         Attr   (�l   hd vsnl   n T�Ŋ\ 
�D92�7��jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek =/Users/davidwalker/erlang/rumbl/deps/plug/lib/plug/builder.exj   Abst  ˃P  h�x��]�k��+)�b;MZ���hI�4q���ۤ��b����etwt�뽻7{��VK�/�/��M�`K6
z��,"����R����ٝ�3{�^I���/���3g~�̙�3��W;���.Z�	j���[a�V�ɲ��fQ�&�� Ũ��f�q��ث��b�����8I��u��,�vˍK�WB��^8輹8�C'��
�����7��_Y�+~2��g�������js�
e즫=L��/�a8��js��[n�Mh}VB���G�F����Ai���?HM����*F�&�d�q�Ysi �"�3��0k���IŜKp7N����=���r%]D�$�_�V@m=�I���Tf*�B̨��8q��LS��0s�
'����Ƌ—���8����^�O�tR��b�2��Q0�SZ�Y�^������ẋx)N���uQ��E���G���+�lC)h҂\׏�bҡ�I���M�V]�::l_��L�R~�s��BHX��[�I�)!.`�Nw���e�y����E�C ���D�j�02E�^�bG��6�n���2�1�"�JeA)R���j�!��l����g�(�����B3�C�x�������*�%!�5�Yn��`�ȁvo����j�n�4�k��$��-i�0dC(=<�!�D@���^����KJ\�jXOA�����h�.Ӵ�o�"����:�x�PPA��;�D�L�������o�p��ø�.�0E�<������Q��"Q�ʫ$���)�(���h�eB��cܳ���(m/�?
���x��p1��4[�x'�ɓ9�>l�q�t�9�f��#IT,��8�t��P�u ���KF�Up�7�^^9ME�ǰ�y�7�4D�,����5��� �~\����t��P����ꮖ)�~��!7Q�����j.�o��Ds���!��(�jJ!�K��5%�5���'��'�qI*YA�Ch��&�x�!Җ�8��'��;MJ�%��K�/�ʗ��"1$�y;�$U���
��S[!NBu�b��Q��K�~xM�$��~I��ߏ�Z�'\��PI���n��t�/PV�4O�dB۸G��J�!�5��%�j-'�IW?�@��8��A� TϬ�%t�|9�S4�0�B��@K�"�ѝ%`%�%���ʖ��o��T��?��Kԙ?�������4�`&�#�Ľyx	B��g��@�a��1Ҩv�6עk+�Z~�*	������c�I�v�H�vf$�Ό	�w�j��j3�T�7���,�cG(xt�	)cH�T�Ց��؂ձ)��B
�2�pd!4!��yk}B�ȕ�hh)�j+��,�9��E>^��©)⦐±)�!�3"�Ph)!�32�Ph(!E9�@��8��A�	C��G��ώ��g����gV~ޱ�����)�����GH�ޑ@���P{oL���P{�Pk���@8�w-�����K���cՊ�Y�y&����s4��J��#<�_�9�E�O�Oc+�T�EwPe������x-�*t15+aj�|g�p?�j �YBً3hL	�=�<ͬ|��	-�c80J[�h.:�?匽�di���x��§b�4<G><�pfeKr:�W0/0����"^F+~<H��v�{z�u���nB�����F"�������$��S�C/���ԑ&s��}�ix�$��9�
-�8�)Ai���g��MfSp\��O+;E�,^�I�{h1<��ġ�-�ݛg�d�i�a&Q�t��z��<S�y`nE)��kT�L���?���Q����Mb�;��ga��/�cv�y˗���)Yy��^y�c=�?��O�{�%�2���$0<���Aw"��&C��ا�>���S0O�Y����lԯ�1��D-E�.K�d�T�<��R}OkHX %�1��#r�o�$�� )� (E���MF�<�P�I�m[�4����Q�4���8�`0���#h�����Y��cX�Y��I�HF�D�u<�j��S�,G:�C�Di`U!լ�jV�C��ݬ�we�'��+��w��C$bx;C���6*ހ�����t�Ji�Z�l��
}d�ЉB���㒥	�p���O(�l=g��l=6��c�lI��.�dRn:֡ǧg]ma�����ZN�'Y/%�<����ً�>�X�,�u�3�*
�ǻ�9��Vӎ߱�V���M&�k�~�U�2J�Z��O�U���:�\-2�H���� ��]�o�*�� �Z0�kڙ��]%�"tNvpZ�0�C`j�3��qk=-%̉�]S�7)�މ��bQ^�H�[i�@�깦&�de��8E�I�k���ʴ9pA]�-�I}�H��h�1�M"r�����	�����S 6׉��H�),n���Q��P�<�2��8�7���f����=�^^=)E+���3����`x�t����lx�������I�oXex�4�a��%�ᕺ����S 6׉(�zqpS@�������9������@�B+{����C�;\���78��:Xu�#)�k�64�X��A��]�P&��h*�&�q��9{������(ne�l-��Z�8jyxɏ��E���\Ѫq�D�,��&-��1�Ʋc\�����h�V:�hR̚4Z�(K7)q�c\3;FHP��,#$er�kU����q��1�I,D�X��n�GO��\'��W{��Dm�s����K^�n��y�N�G���E���J5��t]���{�����s������i�8
e���7�QI��Bh�_a���+�b}�6ޥ��qYۜ��%�U�����@wٲR
Pd�꿶A��#����������X:��m�N�����s4���w�Knٻ�w�h���Q�NZKQ����^��킯Ud�� ��2�t�>(���hX�5	ֲ�pݓ�N����V�)��=�U��gx�D+������:�^���ټ�����[i�Jp:H�je^�&O�|ތ��6�mfmU������9�8&'��}g��U@����wH���u<4���[��G�j����m��<j�Hg0n�������R��Z+Ոќ]���~�㾒Vc��j��}��O�X�3�7A�~n��U�=�9GJ���0��U\�Ul�cf(���6Q�����X��*�у���v0t~`*�A�%�}��>Y�xR�s;=,;}
�,�?�F�HP5�&�>�"A~��|�R�п4�U�DՊ��[^���ۥ�gO�;��>���o��
���S���6ow۫�Le����{�U�m1d�!�^IY�FnOoó ���.l������D�B�.�3�~������5�7���6ög�h�|4k+|ϳ�]S�?j�^ r�툪۩���^��aM:�K�x;�%�����w�;�C���I��&���Ov���h�O*t\�Y�9��i��l��6��ɳV�ʏ����l��woŤǯx�*�'�_���+�����?ˋ��
�Hd�C]Yn�Bހ���*�u����w��f���"*nV���^�E��K�P~��`y�lwh8�M����%�"8�s�|S"T��+�J�U�0��ƿ)b��w�Tߦ&<�s�����:��`nE5o���J�UJ�%l��������U��/���D����eOHJN����b�U�}�[	.FrW�[3�����hkp��S_�ݲb#k���u�^)`��� \T������7�̵2T�VW� �p��ܩ�]�� Js���V�����m&V� Line   n           M   "   	�) ))))	)	g	�	�	�	�	�		�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	� lib/plug/builder.ex  