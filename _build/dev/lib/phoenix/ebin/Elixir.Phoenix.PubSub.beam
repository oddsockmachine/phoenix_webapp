FOR1  �BEAMExDc  ��hd elixir_docs_v1l   hd docsl   hhd 	broadcastaa�d defl   hd serverjd nilhd topicjd nilhd messagejd niljm   #Broadcasts message on given topic.
hhd 
broadcast!aa�d defl   hd serverjd nilhd topicjd nilhd messagejd niljm   _Broadcasts message on given topic.

Raises `Phoenix.PubSub.BroadcastError` if broadcast fails.
hhd broadcast_fromaa�d defl   hd serverjd nilhd from_pidjd nilhd topicjd nilhd messagejd niljm   9Broadcasts message to all but `from_pid` on given topic.
hhd broadcast_from!aa�d defl   hd serverjd nilhd from_pidjd nilhd topicjd nilhd messagejd niljm   uBroadcasts message to all but `from_pid` on given topic.

Raises `Phoenix.PubSub.BroadcastError` if broadcast fails.
hhd 	subscribeaad defl   hd serverjd nilhd pidjd nilhd topicjd nilhd \\jl   hd optsjd niljjjm  bSubscribes the pid to the PubSub adapter's topic.

  * `server` - The Pid registered name of the server
  * `pid` - The subscriber pid to receive pubsub messages
  * `topic` - The topic to subscribe to, ie: `"users:123"`
  * `opts` - The optional list of options. See below.

## Options

  * `:link` - links the subscriber to the pubsub adapter
  * `:fastlane` - Provides a fastlane path for the broadcasts for
    `%Phoenix.Socket.Broadcast{}` events. The fastlane process is
    notified of a cached message instead of the normal subscriber.
    Fastlane handlers must implement `fastlane/1` callbacks which accepts
    a `Phoenix.Socket.Broadcast` structs and returns a fastlaned format
    for the handler. For example:

        PubSub.subscribe(MyApp.PubSub, self(), "topic1",
          fastlane: {fast_pid, Phoenix.Transports.WebSocketSerializer, ["event1"]})
hhd unsubscribeaa�d defl   hd serverjd nilhd pidjd nilhd topicjd niljm   6Unsubscribes the pid from the PubSub adapter's topic.
jhd 	moduledocham  yFront-end to Phoenix pubsub layer.

Used internally by Channels for pubsub broadcast but
also provides an API for direct usage.

## Adapters

Phoenix pubsub was designed to be flexible and support
multiple backends. We currently ship with two backends:

  * `Phoenix.PubSub.PG2` - uses Distributed Elixir,
    directly exchanging notifications between servers

  * `Phoenix.PubSub.Redis` - uses Redis to exchange
    data between servers

Pubsub adapters are often configured in your endpoint:

    config :my_app, MyApp.Endpoint,
      pubsub: [adapter: Phoenix.PubSub.PG2]

The configuration above takes care of starting the
pubsub backend and exposing its functions via the
endpoint module.

## Direct usage

It is also possible to use `Phoenix.PubSub` directly
or even run your own pubsub backends outside of an
Endpoint.

The first step is to start the adapter of choice in your
supervision tree:

    supervisor(Phoenix.PubSub.Redis, [:my_redis_pubsub, host: "192.168.100.1"])

The configuration above will start a Redis pubsub and
register it with name `:my_redis_pubsub`.

You can know use the functions in this module to subscribe
and broadcast messages:

    iex> PubSub.subscribe MyApp.PubSub, self, "user:123"
    :ok
    iex> Process.info(self)[:messages]
    []
    iex> PubSub.broadcast MyApp.PubSub, "user:123", {:user_update, %{id: 123, name: "Shane"}}
    :ok
    iex> Process.info(self)[:messages]
    {:user_update, %{id: 123, name: "Shane"}}

## Implementing your own adapter

PubSub adapters run inside their own supervision tree.
If you are interested in providing your own adapter,  let's
call it `Phoenix.PubSub.MyQueue`, the first step is to provide
a supervisor module that receives the server name and a bunch
of options on `start_link/2`:

    defmodule Phoenix.PubSub.MyQueue do
      def start_link(name, options) do
        Supervisor.start_link(__MODULE__, {name, options},
                              name: Module.concat(name, Supervisor))
      end

      def init({name, options}) do
        ...
      end
    end

On `init/1`, you will define the supervision tree and use the given
`name` to register the main pubsub process locally. This process must
be able to handle the following GenServer calls:

  * `subscribe` - subscribes the given pid to the given topic
    sends:        `{:subscribe, pid, topic, opts}`
    respond with: `:ok | {:error, reason} | {:perform, {m, f, a}}`

  * `unsubscribe` - unsubscribes the given pid from the given topic
    sends:        `{:unsubscribe, pid, topic}`
    respond with: `:ok | {:error, reason} | {:perform, {m, f, a}}`

  * `broadcast` - broadcasts a message on the given topic
    sends:        `{:broadcast, :none | pid, topic, message}`
    respond with: `:ok | {:error, reason} | {:perform, {m, f, a}}`

### Offloading work to clients via MFA response

The `Phoenix.PubSub` API allows any of its functions to handle a
response from the adapter matching `{:perform, {m, f, a}}`. The PubSub
client will recursively invoke all MFA responses until a result is
returned. This is useful for offloading work to clients without blocking
your PubSub adapter. See `Phoenix.PubSub.PG2` implementation for examples.
jAtom     Elixir.Phoenix.PubSub__info__	functionsmodulemacroscalletslookuperlang++applybroadcast_from!errormessage$Elixir.Phoenix.PubSub.BroadcastError	exceptionokbroadcast_from	broadcast	subscribenoneunsubscribe
broadcast!module_infoget_module_info  Code  &          �       � " 0e;e`25BERU0@G @@P@`p�b0� 0@#@�   8�A#9�:�0B 3BCB S4�#+�3@@S@C�0 @@#@�00  �� H��@�@�0�1� @�P@�9�:� B B#+��P0F G�G#E�`0�`@�+�
 ЙPJ��p
@�0�1�`@E33E#33E3#@
0���
@0`@E33E#33E3#@
0���
00`0E#3E33E
3#@
0���
0@3@��
00@0E#3E3#@
0���
0 0��09: B B#+�P0F G�G#E��0��@+
 ��J� 
 @� NP� 
@@� N `  StrT    ImpT   X               	   
      	                  	         	         	      ExpT   |   
                                                                                           LitT   W   �x�c```d``hm���L)�IE��)ɉ�%�� >����ħ��&���Q!��ťI��E�I��H|�<wi��, ��)� LocT               Attr   (�l   hd vsnl   n � ��Z6�]�eV[�]A�jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek B/Users/davidwalker/erlang/rumbl/deps/phoenix/lib/phoenix/pubsub.exj  Abst  ��P  �x��X�n�H6�!#�V� #�m��f�`�Ҝ�2�> jL'�1�ն3��@.Kn��sK�&y������.;�r���������՞��BF̰sy���[��3�$�ՍBJ���2��o9�g�w��B�c|������Q?�����cu��}�����u��{B���i�^$���XC��|ON�|���D�܁y���f������#=�!���7��ᕿ�X;�\a�k�g<���X�������Z.aW�X)��ֈ\��F�m]	�ޅ�ǿ7��w���A��N")� �3:g�;��J~@�x�@��J�·zТC�@�#4|_����0b��zX���
�_�����WX�+$���S�*�v�}��o�/RM��I��':.���Z��I	#���z�
��P#���H����/��d-�T-������^��X�UB�����8*�q`<ή�O���(��B)�u�XC͑����<w!�/�WH8:%u:�{�u:���=��)���SbUDXˋR�T$W�	%���
��X�$��~�E�=}ˡ1�l�L̐;�9H�~hl�ɼ�m��W ��=�'ar� �Xi�W<Ԉ{e'��ว	��H��M�!Q��/	�JS�a�2%�'�ؘ$�����*����2blU��E��;ɩ=稾��|��Iv��e��#�{�Lx��\�!���H�]�*d�s�Nї��a�0q(�U��qd�I�M9�uͯ����Pr����|�Q@�%e_OUc��r{9a����$�MC7$4��g�Fz֝r�QR46���}
��M�f���/*P5��u�y<�TEYP�b��-C���^�(s�{.m/�l���,���E"�O�/,�ϟ��`J�ϑ������ݙ�{�P��cq���,hn��o����  缲D�YHQ�8C�qO���X�p`��^��>�w���Uj���mؗõ����څ�^cP�q���k�J��Uħ3�f��S�����N�i.O�FI�>�'���r�R߻���aO�J�����EhӡI�p;��>	��x�u+�2����A7�P"1&��H�H$~�c�c鸜���c���vc\֍<ƽtq�����>�*6�AE��T����d�*e��bVD2�"c"������5=?�82$�*[�r��EJP��ɺ��Q��ɒ���uV?�X��u�J	I3�K��,c��4C(��l�LeeV�ʬ��Y)+���-����-������Rv[���Aݣ3�����(l�yß��CU�
�s� %EM������)���v)L%�֦�֦�Z�imZ������M���o>D17ew����b������`�w�wH�߁6��	�B��P��]`qH�E�����q��и,������[�%>��o84.ˆF���6  Line   F                 	�	�	�	�	�	�	�		�	�	�	�	� lib/phoenix/pubsub.ex  