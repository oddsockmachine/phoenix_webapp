FOR1  2�BEAMExDc  �hd elixir_docs_v1l   hd docsl   hhd callaaCd defl   hd connjd nilhd arg2jd Elixirjd falsehhd default_configa a%d defjd nilhhd handlersa a-d defjd nilhhd initaa>d defl   hd optsjd niljd falsejhd 	moduledocham  �Socket transport for long poll clients.

## Configuration

The long poll is configurable in your socket:

    transport :longpoll, Phoenix.Transports.LongPoll,
      window_ms: 10_000,
      pubsub_timeout_ms: 2_000,
      transport_log: false,
      crypto: [max_age: 1209600]

  * `:window_ms` - how long the client can wait for new messages
    in its poll request

  * `:pubsub_timeout_ms` - how long a request can wait for the
    pubsub layer to respond

  * `:crypto` - options for verifying and signing the token, accepted
    by `Phoenix.Token`. By default tokens are valid for 2 weeks

  * `:transport_log` - if the transport layer itself should log and, if so, the level

  * `:check_origin` - if we should check the origin of requests when the
    origin header is present. It defaults to true and, in such cases,
    it will check against the host value in `YourApp.Endpoint.config(:url)[:host]`.
    It may be set to `false` (not recommended) or to a list of explicitly
    allowed origins
j Atom  �   e"Elixir.Phoenix.Transports.LongPoll__info__	functionsmodulemacros
sign_token__pubsub_server__termerlangerrorutf8atom_to_binarycryptoElixir.AccessgetElixir.Phoenix.TokensigndispatchhaltedtruemethodElixir.Plug.Connget_req_headerElixir.Enumjoinput_resp_headerok	send_respparamsElixir.Plug.Parserscallgone
put_statusbad_requeststatus_jsonstatusnilfalseElixir.Plug.Conn.StatuscodemapsputElixir.Phoenix.Controllerjson
server_refElixir.Processalive?verify_tokenverifynew_session
serializerstrong_rand_bytesElixir.Baseencode64os	timestamptuple_to_list	byte_sizeall	window_ms-Elixir.Phoenix.Transports.LongPoll.SupervisorElixir.Supervisorstart_child	undefined	forbiddenconfigendpoint_idv1tokenbroadcast_from!selfElixir.Phoenix.PubSubsenddefault_configresume_sessionmake_ref	subscribepubsub_timeout_mshandlerspublishbody_paramsElixir.Phoenix.Socket.Message	from_map!unauthorizedtransport_dispatch
client_reflistenflushnow_availablemessages
no_contentinit__transport__fetch_query_paramstransport_logElixir.Phoenix.Socket.Transport	force_sslcheck_originmodule_infoget_module_info-call/2-fun-0-Code            �   w   � " 0e;e`25BERU0@G @@P@`tp�b0�00@#@@$���� r=����� �G ��  �@r� p �@��  @#@�@@#�   @#@@3@$� @00��0
PМ�� 
S+�S
�� 
StS` cucp� s-s;s`	O�	P	G��c0 yc @G @�@ @@G0�@ P@3@G@@GP#@@3�P0`@#@G`� �`0`@Gp#@G��p0`@G�#@G���0`@G�#@
��0p �c`yc 0P@C@@$�� 
=����G� ��� @
��p @#@��0=9: B B#+
@G�@$@#$�� �@#@3@$@G0+�@
 @$�0 �� �@G�  �c�yc PP@C@3@#$@4@D�� 
=��КG� ��� @
��p @#@4��0=9: B B#+
@#@4#@3@D@^P+�@$#@3@4@C@DP.P@G�#@
"��N0p��J��J��
#   @@� � 
$=!� ��G� ��  @
$�p !,"
%+#
&"@	�#��@@#�@
$�0�@#@	�@@#� �@� �$�
-@% @@3@#)&@#��,&
%+'
&&@ '@ (�
00)00@#@@$�+�* r=,*�+��G �� +@r�p ,@�� @#@�@@#�  @#@@3@$�@�0-�
2P.�P$@t@d@
3@C@34@#D@T�  @$@	��@� �@G�� P�@| #o#| #o#oqm  \p�Z
;� Z
;� @�0t�/t 
=1/�0t��G�  �t� 0@
@t�p 1@@
<@T�  E#E#EE$EE4EDEd$4D@
=� 93:3 B B#+3
+2#
@@
A@t�� � �@G�  2@#D@d@
B#@
C�pPF@G
DGGDG@T#@d�p� 0�@#@
 @@#�! ��!�G� 
E 3�J4�"
F0559 0@#@�7�6 r=86�7�#�G ��# 7@r�#p 8	@3@#�#@ 914@@#�$N :�%
J ;@G<�&
K0=�B�B G3P@@@3@#4@D�'0)9A:A B B#+A
9A#:A#@B# 3B#CB# SB#0c+A3
D@D@
B#@
C@c@S@C$�(p@#@3@$$�(@%@$�) @@$@D�* X+C
@$�+U@F0#G
MGG@$@DD�+05@
N@44�,  @D�->@9?:? B B#+?
M+?#0 F G
G$P?>@>D@�PA@�PB@�C�*HD�.
O E@GF�/
P@G@@@3@#@$@4�I�H 
Q=JH�I�0�G ��0 I@
Q�0p J�0@#@$@3@$�1@N9K:L B +L�@
T@4�@ �2 �@G�  K+L
@
@4�@ �3 �@G�  L�1JM�4
U@NP@@#@$@4@3D�5�O @@$�6UPF@#G
GGG@$@4$4�605@
<@DD�7  @D�8�OOS9R<R@ P0QPB B#+R
+R#@
PQB B#B 3+R�+R300F G�G#PROSOD@GPT�9
VU5VV1T	W�:
M X5\ @�Z�Y r=[Y�Z�;�G ��; Z@r�;p [	@G3@#�;@\1W@
]�<
W@^`@@3$@#4@D@T�=�_ @@D�>U@F0#G
XGG@D@4�>05@
<@$�?  @�@�__g9f<f@ `0d`B B#+f
Y+f#@D�AU@F0#G
XGG@D@44D�A05@
<@$$�B  @D�Cac9b:b0B B#B 3+b
Z,e3bacaD@@
[=hdB B#B 3+f
Z+f3e@#@
=hf_g_@@
[h@D@$4@T�D �@4�jT�iT 
=ki�jT�E�G�  �T�E j@
@TT�Ep k@GT�E  �E�G�@
E
ZD@4 `l�F
\mn�G
 o9n:n0P B DB3B C@@3@
]#@C@$@4�Hp9p:p B@@�I�J@@
_@�K  @@�K @D#@3@4�L@@g @D#@3@4@C@�@�MP@3@$#@C@4P�@p�HHq� 
c r@� Ns� 
ct@@� N  u�M
ev@G�    StrT   PTIONSOSTETphx:lp:  ImpT  �   !   	   
      	                                                                           !      '   (      )   *      +   ,      .   /         1         4      5   6      7   8       	   9      	   :      >   ?      	   G       H   F      	   I      	   L       R   S      H   M         ^      `   _      `   a      `   b      	   d      	   d   ExpT   X      c      t   c       r         o   \      m   O       E   J       ;         FunT         e      v        R�LitT    Ex��S]k1�n��n��kA���
��E
bE*�����Lf3s�$���u~�ş�M��R_v.7��&眛a�3�.M���%�$Bk���	SQk�,�*�o�g���]-;�TN�rj��8�{�r�*���0�� ـsKY����K��DW��4�d{3XJv�yUǶ����t�릠�\$	X;&6Π�Q�u��$>�����⋐?��C�p>F�RUR�@�7^���?�o8	�a
ntZ���A���	��G������G���B,�"��B�W�p�W	#
OoM���>�i(S���f��'8��zhK����]��dU�>]����IH��"�0*�σ�S-�	W{-����'qo�J�s^ؘ��G~�h���* k�*��Ҁ(�Ս'<��J���Q�
���g,�	j}����C�Ap��dݩ����YV�m�G(���lX��{q��u8��CZ��1��W�����r���}\���2م�1�%�>�Ǎ�܅����
O����V�l}K�lDBZ  LocT   �      e      v   W      ^   M      X   V      U   U      N   P      G   K      =   F      5   2      .   0      )   -      %   #                        Attr   ��l   hd vsnl   n �f�=��i�S�ƕ\�jhd 	behaviourl   d Elixir.Phoenix.Socket.Transportjhd 	behaviourl   d Elixir.Plugjhd 	behaviourl   d Elixir.Phoenix.Socket.Transportjj  CInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek P/Users/davidwalker/erlang/rumbl/deps/phoenix/lib/phoenix/transports/long_poll.exjAbst  ��P  l�x��]]�Gv�̯�m�����*RdƋ�"K�z386�A �O������;}�{��̂VX;",� h��˒����FYYZE��d-?�Dy 1Hh�<���Iu���s�NU�?Xs���s�ԩs�:U���jO��~ìGqh6bcf�[����5d�֛q:�nl�5�6��;���Vm��f7���a,t]gՆ��_k`��-ש?�|�s<��(y!z���)#�]w�>`��F:��MD��ˮs�	�vgc�c��n!���n����V�/�=~cڎ9�����nw�Q��$k��4<f�����}j��k$n���uc��{Mgʬ��[�g�ve�ω�umb2��n�����C�l��lv��u ��!��F,��z���1�d�f�w��������3#���&��� ���Ƒ��}�-��v[�/a��:�:5��v%�Aӛm�������%�H�����H�5�[f��b05h�v���<!���\����f�AI˨�����5s�]�u���Zu��S��z��:�u�҃��&Hz#��$�`��<I~T!2)�g�0T�6pR�_CR6f
��\�l	FΔg���홃�k@��&-�P�����g�}il����� �^�&i� �s$�;>:�~W4T��� �PL�l-�`����C������f��xf8˗nÌ�����3q��}�t��|P��	��h:�k,�5b��}�0�n=�֍�g��0��A#1gOnޮ� �J[+N��P#Nd�Q���g[>l����gF�� 7c&,ef+ڟ�Մ=�r_j���a�2І����=[6��(C�v3W���ـ�K�M��Hۍ�ۆ y��7�&����E�rSv\�;'x7�� ���9�>XN�q�eg�	��ɬ��6�I�L�6�^��Xv�tc��I\�[d2��{���i�Q����w�(a�ǳ��^�x�
�ˎ[~:��$֊d��}4l���^�������������vL�`˷=��jsS�M���Sf�0�}E�}���l�b�z�>�}�p��y��^��0��6��0��q��l��mf�l���L9����I��������ގC���$t�(��3�f*�2��7@�]ކvy��]�f����Y�,߂�oUE|�9wЍS@`j��&�}�*�o@�7��Q��:�}�*��*����q�.�'��1%��[�Йr<�M������)��ne�yF�G$8��}H�¢�������O��#Z�/��1l5��G���s���w�jn=�Osh�oLSLof��Luz3Ez|���JR�)�6��'���e\ܺ��w�vr�R瑢�GYg	�-�n�f�-�z���5���jW����f�
��A)�%o��ؼ�k��)(!n,�]���Z��]K]�k��䮥^qׂ�T���nׂ!��h��x��m[m�˧ v�Tl�EY�W`������(�(�R��(�`5��㽭Qh7)/���I*��P����a	��5&�	8[.De~*��䝚,y�~@�IMY�&�S��tME�k��1L���X_��4�("����=e�Z6�4m�i���>���'��r�
�n?��U[+ zW��v}�{Q�ޅ�@PB�Ů"Ћڭ�
��:�c@2л=�RzW�1��>л��m��q��0л�zs5���g��z�KvCA��b}�����������6èwR@��|���S�h$`�	O4��8���T�J���J���-}`��q���H�`l���E���[����n=�ˎ�}�uA�B��t�h_�	د���)`i)�͸��JT }(@К� �ܐ�2\�FJa�U�|�W�Lb\�3sS���	�(ɕ~��2�:Su�3ru)3Cuc�Lz�%4�j�_�1�Ӳ/�i��m�eQ#φ;��j�k���$�j�*��*���ۓ��;��ǲ~�����p�U5gXE�D�V��3���|���8��V�DZ_UpQ���S�aU�0 �V+r���:΀!0g 3Z�*�ܶ��|
`wJ�v[&���g�C'L Ha��v#�����hU&y����M�I ��`��v������vV��E����y,z���!��C�������d9����+��!f��m�8�k(�k�G�ŀ�R����5��H�_Aԯ����4)�P\Ǔ����0��G~�\�(�9$�����)g�c+�'Cb�p ���~�4���b�c1��|�4�w�E���������7�}|��PFLי���]x1��x��b6�CŰ!,�*��ś��o`~���Y�c�\��P��8�Y�͝/0�����_$�ә�l7�����]A(������T�޽�����$%ٕ�n��{{gE�oV���;+
��������ܥ�Z�ERQ���STgEMu0 IuV*R��:+:��!0ա'��n�j�]>�{��%�wV`$��
U�wm��;+V�w�2tv�ŷ�x�yP�k�C�A�4}PQ�>(��!@��e������A�d�C�����B����3͂Й1zY���.��N:��=A���n�<U'^.�}��0�o���w,2�����^�E���n���9�y�i�|g )���؎(B�d��G�v`hY�h��T�r`�N�;y�H�#EV�)-u%	�7/��������،�vxM@c�V?)��'	_�"���?�r��,B�Z���a��|��z��Фa���Ȝ�0�q���C�9K���i#�E��9�"����)X���Z�)�0�f	�d	sY�R��9K��%�-Sn�js\>�;�b�ݖ�^3���5Ge�9:c���x���`
P+sN�WE}��[�pe�@e� AO����r"�
�d��06-ǵ���qE N�?R�Ey��O�O������#em����u۾�n;��0�(�P/❯� �4G�ã�Ê�pr�"HM�娦�r���o�aݱ,��'G���y��S~�o�ł����1~�'�X���f 5<̓�ښ�~,{ә���6��L"�!�p����u8Q��q.�8��.7���qK���qN���\�	����6<Q��Nh\�D��T�Ϣ<�b8�� 9znS�+w^=�M�Ź�h�~�YiE����wi[��'�6܉��.4�]=%eH�G�Ֆu�`�*,C�H��	1ww���n�-�Ɓ'�"c[�̳�fm�HM�gYA��v}r�{Q��2�219�
'j�Vp��-�	$	�rE��TnYG�0&p�䖣`.��\m��gv/Ѷ���YmԼE��lC�RY."ʮ�pH�ǂ�AĤ{���=mD������^��[�(���!�}=���<����}i@������m���RY���~s����Ω�Փ�/�<�s��<��H���Gu8P�B�O���=��UIܦ�T`��z�;���W����s�͵�[C�����[\�.s)�"__I���NU�N}�s����1m��︨8=b��ė�%n�K�a%�h)���%K}��ru���p��ﹰ˖p�zY
�E�݋@�>����Ya�+\�+��ҟ�g��P\�{CqE�i�W4[���^�6a�A�?��b��cs�Nl |,Mg}��cѮ:u��ig�X� �q�rܫ4?�|�F�J\7���U�zW]�Ć�밅�;�:��6|2ܻښ,���n�Όm����~Yas����Wb��x�����5����(V}B�>��UrV$~�#��	��+l�_�_`��pI���wPk�%ɗ˪8��H�O��~ʦ��
�@�O����8��,��V�E��m�{�$�����cB*����Y��-X��eqr�x<�8�j������;��u���B�Y�$�j2��ᬐB��,�r�ɗ�\h�F���?���c/Yׅ���bW�{�&��x��ωȂ��, ��5k�"�]uޮ�f彨��`D�YP�E��
N��ԻjH�*�1�jW���Uc�����}����6��� �+�l�L�V�9�w��rΪm�{��Gp@8G/�s��p�XנEIl��a�eO��y��ڊ�yh��WSQ=�����+�������Ѕ
$![/��I�
m��Z����� p��.hp�6��]���-?d{]���U�F����a߻F��5��]�|������u��_�v��'��EB�A��=n����Ho���y�W�f�J%�����7l�?����
ˀ����"�x���2>�7�R��I�Wl��k �����������8�/�y���7���M�.�/8�ٽ�]�&87���Mh���uS}<s�8���AMo�n�ЭlQ�[P�[z=o)��\�����j���܆���\hjnc�M��%3v�z[��m�����-AKHFفڒXQ�-�{�����KH��(mIA��v=�{QӢ2�1-K
�/j�Vp��/�I?$I�REҏ�T�IG�1&��䖣`���\m��gv/��!�:��U8J[GiUO�\��ׁz?/(%�1��Yw5�;��}�~�w������	��'���QF{3ړry夊͞���IK}��T��LiOA���MiO���(�iZ��jJ{��nM��T
(��(�����g�/�/��� k���jO�K�a��R:~W�f��},�+d�el��k�E�!����
X�<F5��-޺˫�4��3��3e�r���3j9S�/X���B��	���i��'Z�*����ga�Zg#�,�>� �Wڍ�O�F�����\$`a3�9�����J�~@������\�y�}^[���C$DS��PS��W9�|�/���HDf/����y��T�é���|?U�y5-Ǧ��0���y����(����<�R�\\�n���WsqHr���\C������c�������	8�m�9.�؝R1�q�Gs��]a��9e��|��#*R{��E�6L��{�� �>�O���q��Q��E�q�
�iV0N~r����v5�ێ~�g�E\���a;싕��f{�x����.(�����@�4y}�����L �(�'z����4Zvc��~Y���عF;��84l#����ev@;�(`�x�Nȷ�B!�Vͷ�Z�-M����a��c5!@��_�
��:w��/�{������6#�����z�I/Ν`��m$�0��K��&���?��ƑH
,�v5E8�@�/34��"�NdgZ��]�ֻ�!�R|5{�5�� �}  Line   �           �   M   	�	�	O	V	Y	Z	[	\	]	l	n	p	b	v	�	�	�	�	�	�	�	�	�	{	|	�	�	�	�	�	�	�	�	�	�	�	%	�	�	�	�	�	�	�	�	-	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	�	>	C	D	G	H	I	J	K #lib/phoenix/transports/long_poll.ex