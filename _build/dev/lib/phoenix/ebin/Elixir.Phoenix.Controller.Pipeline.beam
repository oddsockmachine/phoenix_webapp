FOR1  BEAMExDc  �hd elixir_docs_v1l   hd docsl   hhd __before_compile__aasd defmacrol   hd envjd niljd falsehhd 	__catch__aa�d defl   hd kindjd nilhd reasonjd nilhd 
controllerjd nilhd actionjd nilhd stackjd niljd falsehhd 	__using__aaWd defmacrol   hd  jd Elixirjd falsehhd plugaa�d defmacrol   hd plugjd niljm   ;Stores a plug to be executed as part of the plug pipeline.
hhd plugaa�d defmacrol   hd plugjd nilhd optsjd niljm   RStores a plug with the given options to be executed as part of
the plug pipeline.
jhd 	moduledocham  �This module implements the controller pipeline responsible for handling requests.

## The pipeline

The goal of a controller is to receive a request and invoke the desired
action. The whole flow of the controller is managed by a single pipeline:

    defmodule UserController do
      use Phoenix.Controller
      require Logger

      plug :log_message, "before action"

      def show(conn, _params) do
        Logger.debug "show/2"
        send_resp(conn, 200, "OK")
      end

      defp log_message(conn, msg) do
        Logger.debug msg
        conn
      end
    end

When invoked, this pipeline will print:

    before action
    show/2

As any other Plug pipeline, we can halt at any step by calling
`Plug.Conn.halt/1` (which is by default imported into controllers).
If we change `log_message/2` to:

    def log_message(conn, msg) do
      Logger.debug msg
      halt(conn)
    end

it will print only:

    before action

As the rest of the pipeline (the action and the after action plug)
will never be invoked.

## Guards

`plug/2` supports guards, allowing a developer to configure a plug to only
run in some particular action:

    plug :log_message, "before show and edit" when action in [:show, :edit]
    plug :log_message, "before all but index" when not action in [:index]

The first plug will run only when action is show and edit.
The second plug will always run, except for the index action.

Those guards work like regular Elixir guards and the only variables accessible
in the guard are `conn`, the `action` as an atom and the `controller` as an
alias.

## Controllers are plugs

Like routers, controllers are plugs, but they are wired to dispatch
to a particular function which is called an action.

For example, the route:

    get "/users/:id", UserController, :show

will invoke `UserController` as a plug:

    UserController.call(conn, :show)

which will trigger the plug pipeline and which will eventually
invoke the inner action plug that dispatches to the `show/2`
function in the `UserController`.

As controllers are plugs, they implement both `init/1` and
`call/2`, and it also provides a function named `action/2`
which is responsible for dispatching the appropriate action
after the plug stack (and is also overridable).
j Atom  �   '"Elixir.Phoenix.Controller.Pipeline__info__	functionsmacrosmoduleMACRO-__using__
MACRO-plugwhentrueplugElixir.Macroescape{}plugs@MACRO-__before_compile__termerlangerrorElixir.Moduleget_attributeElixir.Plug.Buildercompile=	__block__dophoenix_controller_pipelinedefp	__catch__function_clause
__struct__Elixir.Plug.Connaction
controller Elixir.Phoenix.ActionClauseError	exceptionraisemodule_infoget_module_info   Code            �      	� " 0e;e`25BERU0@G @@GP@`p�b �@G �� r �9�:�0B B #+��8�#A#3C8�CACSc4�c@@S#@30�@�#@@0��0r0�9�#:�#0B# B# 3+��8�3A3CS8�SAScs4�s@c#@@C0�@@#@�#0�@�0 0@@#@�P EEEF0G�GGEF0G�GGEF0G�GG0G �`
  @�� R=��p�G@  
�p@@R�pp @��p   EGP@G`#@� ��009: 5B B#EGpEEG�E##EG�##EG�##EG�##EG�##F03G
GGE3#F0#G
GGF G
G#EEF0#G
GG�GE#F0G
GG0GEEG�EG�F0G
GG ��H��
P+
+
8CACSc9S:S@BS cBSsBS �8�A������� 
�+�
 +c#+s3�P@CF G
!G3EF 3G
"G#E3��@@@#@
��0P@C#��N0P� 
& @� N`� 
&@@� N p   StrT    ImpT   d                                          #   $         %         '         '   ExpT   d      &         &                                           
                  LitT  b  x��XOo�0O���:6B���7v��2&&M�-7�Z�nl9Ni�C>G$>��������I���P'KUv�2���Ͽg�q�8�s?e���g!+���l���]��K�|>��Y�$1����ۂ%���H��=y�v��?F(Ԃ����R�4}��"38�O�Qy|1�$���W�!9c�� �FD3t�Dp	�9��aa�V8 #<�<���X�(�I�P�y�,��+�b���@��-�UpXk�H{����"g�
v��Y��� a$��J2��"a�$$
d#��>B��^�{
Y��YP������I°����	�B�hꏮ�kC0~x�j������j�]&�FT�A�b_Qm�P�d��P|�`��
���^�}��1c�3l:e���thc����1����#5��ha!#$�b�8��Ӳ[�N��f��c�uk]�9FO$*;j��0��U�!j�\�2�haZV��A^�}Db��r;�JnS�����'?�-eq��w�'�2�~q�u�{c���d�Ҳ\����b�K<�ð8aGP�|J��0��Ȣ�ʑO>���ey�`:	��s�޼ �,U�<D��|b�������+�v�`��J�hzc2��Aн�zƍXX�p��au2�	6����֞b�hw`�}M�a�M� ���j{s-�����N��/�[�~�O6�������E��}�ӊH�����C�Ȱ��* 񔜗�]k�ĉ r+F�n^����I���?�Q`�i$����D�Yb�'��W�l��4�QE,x=
75�������<Vd�����XI쓰��/�va�����9  LocT         
      Attr   (�l   hd vsnl   n �m}"�o�v�!��E�jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek O/Users/davidwalker/erlang/rumbl/deps/phoenix/lib/phoenix/controller/pipeline.exj Abst  a�P  Rx���n�FT��Nl��@/m����I]�#A�"���K�)��������S#>	�S�����kg��p��DQ���'����ߛ�=�[�*����z��oV*��ݺVل�����!X�*-��9���ʎe�{6�{����ԪV��y��q垩�8]Y��ն<l�&��A�n�=t���͞���ҽS?��޳ ��>G��,D���P���.������ϟ����l�@( �F�w�a�խN�
���w� �6P��vɊ���n��k��P�� �[KL�<2����a�#ߢКm9�hD������lE��8���oq���(��37	'�G�'F���T���1#F���l�<�n�n�ĞZ��uD�@ԛF��	MՄ�K�= ʅ���0��%��.�ЉS�ʚ���	�t�C�a6�����b� �w\ڟ�}>�D�E	#�<�:��.�����%�C2Mv�1c���h!h��D�ʝ��zy��"+Ckv^pe1l/��?�����sX�좞���ۦ�Ss�1������=[tK@'���(J�nQ"l;$�3�2�"n�Q�u��%�!4u�"�����DA�E���Q�F�$���ߌw�y	t�
�ߊ˛��O2��2$��A��C��%�w3�:�D�0�g�L��u�ç?���@�s�Xt�r�Ѣ�܂���|zHd��JKD�d��̳A0�4R�W����_v����Y]�t�ĺ&d'�IRZ	gi�.��W�a���lM���ٳfv��f=r��-f!�&r�O:S��.�?,��7}G#�E��c#��P��-gʖ���`���%e��ߕ�����U����q�����5z,P�S�@q|/;��P��Y3EX�ǟY�s'�gx*������_�=8����Qf����Iq�/����Z���?
-K���f�<�r�w-7J%��#��`;AG��^�q���焙�>f��{��ے�J�İ�fn����;d��c]�m�BXnVa��J�^���${]
�oC9�29�D��?�V����y�V�<y��<	9|�Wx�U�w�h�II?�>gt��C��d\�"ц�.&�N��8Gѻ���� �bHm@�e��g�ݼ�Sl}�����և�b-��p�j�&�SV���\d�}ӎy1ʐy������f�!?Ƥ?fj��5��P�E/et�Y��LӸ�4R,_�,_e�|�Fw%�<���!���R_صL�:M�:��kI9��֠� ��(�P9�6���ih���5ܷ>яM�z������P�C�:�P��U���a#B��G�W$]��f�rKWN�nV�`Y���T�F���B�O���`����7�	4l��I���O/�����E�]|�.~�/�}O|� �� /nn
t�tQ�?�'R���M�-ۃ�y>�%_���L�´ș��cE"
��7dW���d�Y�H�y�L lB�#�tֳ�����tj���*��k�	����Q��+��wy�P�Pmb�r��(�c4ȴ��u�^�n��x�	��Y,RR�|+Fd?3�3�}�#�;	{�mm@ݔ����K���� Bv�G_75��	����ڏ�P�䤋�-�iw �z����P��H9x�f3Q�{��j�ݖn�3T����s�n��m�5&���瑩�2��XF#���W�������)!�nisy���?��ު��g�ޝ3W��a���j��b�=��KR^(���[9��wnٓE��^�m�T����G���������xhG�͝��e���b\�hd�7+o����t���̮��_q�H��v�?�řv�L�����;T��۳N7�dqk�����έ,�������%��W����+�?��:�^QHC�����ڍ��`g���G�P{����U��lU��h�i��T�=�-ܞ�׺O�;y�pK���u@b?�iW���	ȩ�sg��=*��=���^S��c��\$��TV�U0��u��ZF=˨g2�d���P�PwQ:�B7+���D`=f��3W<�����ut�Z}�5m���Hb\��q��!#�9T�E�.e�6�t�74�^�*;�PE?r>\��)=3��i�ϳ�>�0���-8�   Line   O                 	W	�	�	�	�	s	u	v	�	�	� "lib/phoenix/controller/pipeline.ex 