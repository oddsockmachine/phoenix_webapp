FOR1  �BEAMExDc  ؃hd elixir_docs_v1l   hd docsl   hhd signaaGd defl   hd contextjd nilhd saltjd nilhd datajd nilhd \\jl   hd optsjd niljjjm  �Signs your data into a token you can send down to clients

## Options
  * `:key_iterations` - option passed to `Plug.Crypto.KeyGenerator`
    when generating the encryption and signing keys. Defaults to 1000;
  * `:key_length` - option passed to `Plug.Crypto.KeyGenerator`
    when generating the encryption and signing keys. Defaults to 32;
  * `:key_digest` - option passed to `Plug.Crypto.KeyGenerator`
    when generating the encryption and signing keys. Defaults to `:sha256';
hhd verifyaaad defl   hd contextjd nilhd saltjd nilhd tokenjd nilhd \\jl   hd optsjd niljjjm  uDecrypts the token into the originaly present data.

## Options

  * `:max_age` - verifies the token only if it has been generated
    "max age" ago in seconds. A reasonable value is 2 weeks (`1209600`
    seconds);
  * `:key_iterations` - option passed to `Plug.Crypto.KeyGenerator`
    when generating the encryption and signing keys. Defaults to 1000;
  * `:key_length` - option passed to `Plug.Crypto.KeyGenerator`
    when generating the encryption and signing keys. Defaults to 32;
  * `:key_digest` - option passed to `Plug.Crypto.KeyGenerator`
    when generating the encryption and signing keys. Defaults to `:sha256';

jhd 	moduledocham  ZTokens provide a way to  generate, verify bearer
tokens for use in Channels or API authentication.

## Basic Usage

When generating a unique token for usage in an API or Channel
it is advised to use a unique identifier for the user typically
the id from a database. For example:

    iex> user_id = 1
    iex> token = Phoenix.Token.sign(endpoint, "user", user_id)
    iex> Phoenix.Token.verify(endpoint, "user", token)
    {:ok, 1}

In that example we have a user's id, we generate a token and send
it to the client. We could send it to the client in multiple ways.
One is via the meta tag:

    <%= tag :meta, name: "channel_token",
                   content: Phoenix.Token.sign(@conn, "user", @current_user.id) %>

Or an endpoint that returns it:

    def create(conn, params) do
      user = User.create(params)
      render conn, "user.json",
             %{token: Phoenix.Token.sign(conn, "user", user.id), user: user}
    end

When using it with a socket a typical example might be:

    defmodule MyApp.UserSocket do
      use Phoenix.Socket

      def connect(%{"token" => token}, socket) do
        # Max age of 2 weeks (1209600 seconds)
        case Phoenix.Token.verify(socket, "user", token, max_age: 1209600) do
          {:ok, user_id} ->
            socket = assign(socket, :user, Repo.get!(User, user_id))
            {:ok, socket}
          {:error, _} ->
            :error
        end
      end
    end

In this example the phoenix.js client will be sending up the token
in the connect command.

`Phoenix.Token` can also be used for validating APIs, handling
password resets, e-mail confirmation and more.
jAtom  (   2Elixir.Phoenix.Token__info__	functionsmodulemacrossignElixir.Phoenix.Utilsnow_msdatasignederlangterm_to_binary"Elixir.Plug.Crypto.MessageVerifierget_endpoint
__struct__Elixir.Phoenix.SocketElixir.Plug.ConnendpointtermerrorElixir.Phoenix.Controllerendpoint_moduleverify
get_secretconfigsecret_key_basekey_iterationsElixir.Keywordget
key_length
key_digestsha256digestlength
iterationsElixir.Plug.Crypto.KeyGeneratorgeneratemax_ageElixir.Accessnilfalse*truncokbinary_to_term=:=true+module_infoget_module_infoCode  :          �   (   	� " 0e;e`25BERU0@G @@P@`'p�b@�5u0@@3@@#$� �@#@� � 0@�0  �@�G@���P@�`   ��p������ �0�;�@
�
�� ���� 
 ������G  
��0�@
��q  ��pN@�0��b0@3@���
0@3@��
0@0@4@#$@@
#@
��p@@
@i�#@$��0P@@
@	 #@$��0P@3@
@
 #@$@3$��0P�F 3G
!GE3G03F CG
"G$EC33F CG
#GEC3#@4@��0`@�
@5!5!#0@@@3@#$��@#@�0@#@
&@@#� p,
(+
)@
(=�}�i��|�@#@@$@#$� �9 :# B B#+#
,@#���"�"@��,$
(�$
)@3+3
/@$=�} �$$�  '$=,
(+
)0 F G
,G0@G@0 +#
@GP0!+#
(5@G`"�H#�J$� 
1 %@� N�&� 
1'@@� N �  StrT    ImpT   �                                                             $   %      '            *         +                  -         .         0         2         2   ExpT   X      1      '   1       %                                             LitT   �  x�mN1�04�P��`�̬��B�Q�����I�v��IRx��g��  �Ө}O����Ri��#���6L�gH�^�Lu�(Q�{�����}��`-��H��msM=��C�\��䁃���d��!����Y��{�޲�nˠ�!�~�'8�d�7���gA�+4�Y<�Z2�J�ILocT                        
Attr   (�l   hd vsnl   n q�vi��$3w�@jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0hd timehb  �aaaaahd sourcek A/Users/davidwalker/erlang/rumbl/deps/phoenix/lib/phoenix/token.exj   Abst  	�P  `x��Y�o�DwΗK�����G�R�
��P��m"$>^�����g[�/�K�|���f����z��Z�'�xv�7���LG�����9.ʜ��w�;��`:���,sv�()qg�O��i�<JR�,��c�,�K�u#p�'�7͜�1;~�MS���2��d�.�F��d�.��˘-Y���8��;?d�K��Cݜ��<�%'��p�9.X�W=������d%�͏qd�_dԟ��\Y}\����=Y$�:Ҩ[Y��e�s���:�5u�N�t���R{����Da��y��%7���ɜ�yj�*�GQ^����:�Ya��\d1$�%�*������� �zFƜa��dQ@f
��;#�$~�3
P��H;Y%�l���!͉C�胎���S���� Hd7a1q�;\W�F�5����~e��y�)�+GV遃G�XX/�~"TV��%�H�}���a_$V#8��K�,_�a� �I
HIR��ŋ=a�a�>��t�B��׽H�$��q���YB�n��	G�\�朔��H����~N9h��@� �퐖�`��l|8M�,����`E�l:�@y~%߿IsZ$��!��a���U�|��'�tݜd��G;��;a4<R�/p�z摕�^�P�}t��ʀ7؀7��G�bԞ���$}�͋��ϕ�#,�H�u#0^���䫬Lw�O����\��E!�Ie�D��Ԫ���!�Y���V�긯dP�+�Y�R0I�=F��z�F�T3:M��+� (�Q�+�E]�FX@f��q���w{=ѐ�����'�LO�IO�u{�l�'>7����3Z*w��(V4�!=����f0+N���!�B�aM�'{�F�ޭs�������98�-���o�Ϛ�7i����ickK�Z���:ܯ��%?@�C��ӌ׭!m*� :bz��1o�����A��v���������kG^�c���_�s>���cf���c�q�㌫MD��P#�{&�ŹhBi���p�h�4M!h;kC�7c�2Fi��\hC�Ťݤ-Θl�Oj|����Oȥ��τn��;�Ǝ��K���)�pե�����=��j�Pw%%_�h���I��a(Wʕ&���ko�<P�@�6�*RQ;F)��jt������kgL���b����0��6��v���Pk,q_a�7R��XHm���0o�������)�C�<�������>W���n��kDi�Ɔ��[I��y)��J��(
ål�ğR��Ͱ���*��8����/-�?�٧	�V�&�[�
2Ճ~km�ٓn�������j���}}����ui��P4�ڋ7(�Yu�$��Ty�Ǟ����~�.��ECvc�e�$_`]�Q�}���׳��E?hG(�.�T���gN�l���JА���Q�6h_�}���ú"~�}Xg�+�������Zo�J_cۣ�=�N�JrN�"������YM1��u�7O ���p\Rԏ|�k�K=~m�0�.��7����/����� ��b0�f��P��T�CKK	�G+�S���.?JgR�_���<B���R�lWTb�f�x���u]Qَ6WT��=A���ng�9 "�&������g���<Ï��h-��tᳪ �Z�o�*��QL������*>U?pU{J��O�Ƿ�]Ʃ�Y*~3��������x��g��gkz�Y����d"+&�������?t����C�n��t�3�Hi��-ǖ�2�zRws�C���,9%1k���ǻ��j�wmzhakx����,p�/��Ԣ{�RP����KB��  �r   Line   U           %      	G	H	L	J	M	N	y	z	a	~		�	�	�	�	c	d	e	g	i	k lib/phoenix/token.ex   