defmodule Rumbl.Auth do
  import Plug.Conn

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    user_id = get_session(conn, :user_id)
    user = user_id && repo.get(Rumbl.User, user_id)
    IO.inspect(user)
    assign(conn, :current_user, user)
  end

  def login(conn, user) do
    IO.inspect(user)
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  import Comeonin.Bcrypt, only: [checkpw: 2]
  def login_by_username_and_pass(conn, username, given_pass, opts) do
    repo = Keyword.fetch!(opts, :repo)
    user = repo.get_by(Rumbl.User, username: username)
    IO.inspect(given_pass)
    IO.inspect(user)
    cond do
      user && checkpw(given_pass, user.password_hash) ->
        {:ok, Rumbl.Auth.login(conn, user)}
      user ->
        {:error, :unauthorized, conn}
      true ->
        {:error, :not_found, conn}
    end
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

  import Phoenix.Controller
  alias Rumbl.Router.Helpers
  def authenticate_user(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in to access that page") |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end

end
